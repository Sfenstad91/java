package no.ntnu.imt3281.project1;
/**
 * @author Martin, Stian and Cim
 * 
 * This class handles all components of label. It also initializes the variables with the constructor label.
 * It sends out the definition and layoutCode for label.
 * 
 *
 */
public class Label extends BaseComponent {
	
	/**
	 * Constructor for Label BaseComponent. Which initializes the components.
	 * @param component
	 */
	public Label(BaseComponent component) {
		row = component.row;
		col = component.col;
		rows = component.rows;
		cols = component.cols;
		anchor = component.anchor;
		text = component.text;
		fill = component.fill;
		type = component.type;
		variableName = component.variableName;
	}
	/**
	 * Constructor for Label. Initializes the variables in label.
	 */
	public Label() {
		row = 1;
		col = 1;
		rows = 1;
		cols = 1;
		anchor = 10;
		text = "";
		fill = 0;
		variableName = "component" + (nextComponentID -1);
		type = "JLabel";
	}

	private static final long serialVersionUID = 7777;
	/**
	 * Initializes the definition of label component and creates a new JLabel.
	 * @return definition which includes the string given in the method.
	 */
	@Override
	public String getDefinition() {
		return "\tJLabel " + variableName + " = new JLabel(\"" + text + "\");\n";
	}
	/**
	 * Initializes the layoutCode for label and sends all the variables in the string it sends. This includes the getCol,
	 * getRow, getCols, getRows, getAnchor, getFill and variableName.
	 * @return layoutCode which includes all the data needed to set the layoutCode.	
	 */	
	public String getLayoutCode() {
		String layoutCode = "\t\tgbc.gridx = "+getCol()+";\n" + "\t\tgbc.gridy = "+getRow()+";\n"
                + "\t\tgbc.gridwidth = "+getCols()+";\n" + "\t\tgbc.gridheight = "+getRows()+";\n"
                + "\t\tgbc.anchor = "+getAnchor()+";\n" + "\t\tgbc.fill = "+getFill()+";\n"
                + "\t\tlayout.setConstraints(" + variableName + ", gbc);\n"
                + "\t\tadd(" +variableName +");\n";
		return layoutCode;
	}
	
}
