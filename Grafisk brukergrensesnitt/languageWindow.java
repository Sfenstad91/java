package no.ntnu.imt3281.project1;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.CountDownLatch;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

/**
 * This class constructs a new window that prompts the user for language preferred in application.
 * This class uses JDialog as its window and multiple JButtons for inputs. Each button has an actionlistener that refers to its language.
 * @see java.util.concurrent.CountDownLatch
 * @see javax.swing.JDialog
 * @see javax.swing.JButton
 * @author Martin, Cim, Stian
 *
 */
public class languageWindow {
	JDialog langWindow;
	JButton eng;
	JButton nor;
	String valg;
	/**
	 * This Constructor for languageWindow starts the language prompted window.
	 * @param signal is a countdown signal for main that decrements so the application can move on if either button is pressed.
	 */
	public languageWindow(CountDownLatch signal) {
		langWindow = new JDialog();
		eng = new JButton("English");
		nor = new JButton("Norsk");
		
		langWindow.setLayout(new GridBagLayout());
		GridBagConstraints left = new GridBagConstraints();
		left.anchor = GridBagConstraints.EAST;
		GridBagConstraints right = new GridBagConstraints();
		right.gridwidth = GridBagConstraints.REMAINDER;
		
		langWindow.add(new JLabel("Choose a language / Velg ett språk:"), right);
		langWindow.add(new JLabel("\n"), right);
		langWindow.add(eng, left);
		langWindow.add(nor, right);
		langWindow.setSize(320, 240);
		langWindow.setLocationRelativeTo(null);
		langWindow.setVisible(true);
		
		eng.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				valg = "en_US";
				signal.countDown();
				langWindow.dispose();
			}
		});
		nor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				valg = "no_NO";
				signal.countDown();
				langWindow.dispose();
			}
		});
		langWindow.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
}
