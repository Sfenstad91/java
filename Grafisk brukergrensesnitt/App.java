package no.ntnu.imt3281.project1;

import java.util.concurrent.CountDownLatch;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
/**
 * This Class contains main and runs the application. It checks first by starting a window to choose language
 * then it starts the program with the correct language.
 * @author Martin, Stian and Cim
 *
 */
public class App 
{
	/**
	 * This main starts the application
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 *Creates a countdownsignal for language input before calling the
		 *Closes program if no language is chosen
		 */
		CountDownLatch signal = new CountDownLatch(1);
		languageWindow lWind = new languageWindow(signal);
		
		try {
			//waits for either language to be called
			signal.await();
			
			if(lWind.valg == "en_US")
				I18n.SetLanguage("en_US");
			else if(lWind.valg == "no_NO") 
				I18n.SetLanguage("no_NO");
			
			tableWithTableModel demo = new tableWithTableModel();
			demo.setVisible(true);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
