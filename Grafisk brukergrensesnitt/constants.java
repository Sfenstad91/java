package no.ntnu.imt3281.project1;
/**
 * Holds constant values generally used for anchor and fill. 
 * 
 * @author Cim
 *
 */
public class constants {
	/**
	 * A list of picture paths used for viewing anchor pictures
	 */
	public static String[] anchorPictures = new String[] {
			"pictures/graphics/anchor_center_shifted.png",
			"pictures/graphics/anchor_north_shifted.png",
			"pictures/graphics/anchor_northeast_shifted.png",
			"pictures/graphics/anchor_east_shifted.png",
			"pictures/graphics/anchor_southeast_shifted.png",
			"pictures/graphics/anchor_south_shifted.png",
			"pictures/graphics/anchor_southwest.png",
			"pictures/graphics/anchor_west.png",
			"pictures/graphics/anchor_northwest.png"};
	
	/**
	 * A list of picture paths used for viewing fill pictures
	 */
    public static String[] fillPictures = new String[] { 
    		"pictures/graphics/skaler_ingen.png",
    		"pictures/graphics/skaler_begge.png",
    		"pictures/graphics/skaler_horisontalt.png",
    		"pictures/graphics/skaler_vertikalt.png"};
}
