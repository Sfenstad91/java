package no.ntnu.imt3281.project1;

import java.util.Locale;
import java.util.ResourceBundle;
/**
 * This class is for internationalization. It collects the data from the different language bundles and
 * sends the correct language to the application.
 * @author Martin, Stian and Cim
 *
 */
public class I18n {
	
	private static I18n i18n = new I18n();
	private static ResourceBundle bundle;
	/**
	 * Constructor for I18n.
	 * @see java.util.Locale#setDefault(Locale)
	 * @see java.util.ResourceBundle#getBundle(String, Locale)
	 */
	public I18n() {
		Locale newLocale = new Locale("en_US");
		Locale.setDefault(newLocale);
		
		I18n.bundle = ResourceBundle.getBundle("no.ntnu.imt3281.project1.i18n", Locale.getDefault());
		
	}
	/**
	 * This method gets the language from input sendt from App. It then sets
	 * the correct language (English or Norwegian in this case).
	 * @param language containing English or Norwegian in this application
	 */
	public static void SetLanguage(String language) {
		Locale locale = new Locale(language);
		
		I18n.bundle = ResourceBundle.getBundle("no.ntnu.imt3281.project1.i18n", locale);
	}
	/**
	 * This method gets the correct bundle from what language was chosen, and returns the bundle.
	 * @return the correct bundle of translations (English or Norwegian).
	 */
	public static ResourceBundle getBundle() {
		return bundle;
	}
}
