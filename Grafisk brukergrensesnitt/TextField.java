package no.ntnu.imt3281.project1;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * This class handles all components of TextField. It also initializes the variables with the constructor TextField.
 * It sends out the definition and layoutCode for TextField.
 * @author Martin, Stian and Cim
 *
 */
public class TextField extends BaseComponent {

	private static final long serialVersionUID = 7777;
	
	int width;
/**
 * Constructor for TextField BaseComponent. Which initializes the components.
 * @param component
 */
	public TextField(BaseComponent component) {
		row = component.row;
		col = component.col;
		rows = component.rows;
		cols = component.cols;
		anchor = component.anchor;
		text = component.text;
		fill = component.fill;
		type = component.type;
		variableName = component.variableName;
	}
/**
 * Constructor for TextField. Initializes the variables in TextField.
 */
	public TextField() {
		row = 1;
		col = 1;
		rows = 1;
		cols = 1;
		anchor = 10;
		text = "";
		fill = 0;
		variableName = "component" + (nextComponentID -1);
		type = "JTextField";
	}
/**
 * Sets a predetermined value to width with the parameter i, or changes the value when changed by user.
 * @param i which is 0 or set by user.
 */
	public void setWidth(int i) {
		width = i;
		
	}
/**
 * Gets the value of width and returns it. This value is either 0 by default or changed by the user.
 * @return width which is 0 or set by user.
 */
	public int getWidth() {
		return width;
	}
/**
 * Initializes the layoutCode for TextField and sends all the variables in the string it sends. This includes the getCol,
 * getRow, getCols, getRows, getAnchor, getFill and variableName.
 * @return layoutCode which includes all the data needed to set the layoutCode.	
 */	
	public String getLayoutCode() {
		String layoutCode = "\t\tgbc.gridx = "+getCol()+";\n" + "\t\tgbc.gridy = "+getRow()+";\n"
                + "\t\tgbc.gridwidth = "+getCols()+";\n" + "\t\tgbc.gridheight = "+getRows()+";\n"
                + "\t\tgbc.anchor = "+getAnchor()+";\n" + "\t\tgbc.fill = "+getFill()+";\n"
                + "\t\tlayout.setConstraints(" + variableName + ", gbc);\n"
                + "\t\tadd(" +variableName +");\n";
		return layoutCode;
	}
/**
 * Initializes the definition of TextField component and creates a new JTextField.
 * @return definition which includes the string given in the method.
 */
	@Override
	public String getDefinition() {
		return "\tJTextField " + variableName + " = new JTextField(\"" + text + "\", " + width + ");\n";
	}
	/**
	 * The method for specialEditor TextField. This creates all the elements in specialEditor and creates
	 * all the buttons needed. Sets the layout and follows the action performed.
	 * @return window which leads us back to our table
	 */
	@Override
	public JDialog getSpecialEditor() {
		/*
		 * Creating special editor and the elements it contains
		 */
		JPanel panel = new JPanel();
		JDialog window = new JDialog();
		JButton enter = new JButton(buttons[0]);
		JButton exit = new JButton(buttons[1]);
		TextField tempComponent = this;
		SpinnerNumberModel widthRange = new SpinnerNumberModel(((TextField) tempComponent).getWidth(), 0, 100, 1);
		JSpinner width = new JSpinner(widthRange);
		/*
		 * Logic used to place components in special editor
		 */
		panel.setLayout(new GridBagLayout());
		GridBagConstraints left = new GridBagConstraints();
		left.anchor = GridBagConstraints.EAST;
		GridBagConstraints right = new GridBagConstraints();
		right.weightx = 2.0;
		right.gridwidth = GridBagConstraints.REMAINDER;
		
		panel.add(new JLabel(specialEditor[0]), left);
		panel.add(width, right);
		panel.add(enter, left);
		panel.add(exit, right);
		
		window.add(panel);
		
		enter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((TextField) tempComponent).setWidth((Integer)width.getValue());
				window.dispose();
			}
		});
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.dispose();
			}
		});
		return window;
	}
}
