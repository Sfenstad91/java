package no.ntnu.imt3281.project1;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import no.ntnu.imt3281.project1.TextArea;
/**
 * This class handles all components of TextArea. It also initializes the variables with the constructor TextArea.
 * It sends out the definition and layoutCode for TextArea.
 * @author Martin, Stian and Cim
 *
 */

public class TextArea extends BaseComponent{

	private static final long serialVersionUID = 7777;
	
	int textRows;
	int textCols;
	boolean wrap;
/**
 * Constructor for TextArea BaseComponent. Which initializes the components.
 * @param component
 */	
	public TextArea(BaseComponent component) {
		row = component.row;
		col = component.col;
		rows = component.rows;
		cols = component.cols;
		anchor = component.anchor;
		text = component.text;
		fill = component.fill;
		type = component.type;
		variableName = component.variableName;
	}
/**
 * Constructor for TextArea. Initializes the variables in TextArea.
 */	
	public TextArea() {
		row = 1;
		col = 1;
		rows = 1;
		cols = 1;
		anchor = 10;
		text = "";
		fill = 0;
		variableName = "component" + (nextComponentID -1);
		type = "JTextArea";
	}
/**
 * Initializes the definition of TextArea component and creates a new JTextArea.
 * @return definition which includes the string given in the method.
 */
	@Override
	public String getDefinition() {
		return "\tJTextArea " + variableName + " = new JTextArea(\"" + text + "\", " + textRows +", " + textCols + ");\n";
	}
/**
 * Sets the TextRows with the input from user using the SpecialEditor.
 * @param i sets the value of textRows given by the user in specialEditor.
 */
	public void setTextRows(int i) {
		textRows = i;
	}
/**
 * Returns the value of textRows set by the user using the specialEditor.	
 * @return value set by user.
 */
	public int getTextRows() {
		return textRows;
	}
/**
 * Sets the TextCols with the input form user using the SpecialEditor.
 * @param i sets the value of textCols given by the user in specialEditor.
 */
	public void setTextCols(int i) {
		textCols = i;
	}
/**
 * Returns the value of textCols set by the user using the specialEditor.	
 * @return value set by user.
 */
	public int getTextCols() {
		return textCols;
	}
/**
 * Sets the value of the boolean wrap set by user, true or false. This is set by the specialEditor.
 * @param b is either 1 or 0 (true or false)
 */
	public void setWrap(boolean b) {
		wrap = b;
	}
/**
 * Returns the value in wrap, 1 or 0(true or false). Set by the user using the specialEditor.
 * @return wrap which is 1 or 0(true or false).
 */
	public boolean getWrap() {
		return wrap;
	}
	/**
	 * The method for specialEditor TextArea. This creates all the elements in specialEditor and creates
	 * all the buttons needed. Sets the layout and follows the action performed.
	 * @return window which leads us back to our table
	 */
	@Override
	public JDialog getSpecialEditor() {
		JPanel panel = new JPanel();
		JDialog window = new JDialog();
		JButton enter = new JButton(buttons[0]);
		JButton exit = new JButton(buttons[1]);
		TextArea tempComponent = this;
		SpinnerNumberModel colRange = new SpinnerNumberModel(((TextArea) tempComponent).getTextCols(), 0, 100, 1);
		JSpinner columns = new JSpinner(colRange);
		SpinnerNumberModel rowRange = new SpinnerNumberModel(((TextArea) tempComponent).getTextRows(), 0, 100, 1);
		JSpinner rows = new JSpinner(rowRange);
		JCheckBox useWrap = new JCheckBox(specialEditor[3]);
		/*
		 * Logic used to place components in special editor
		 */
		panel.setLayout(new GridBagLayout());
		GridBagConstraints left = new GridBagConstraints();
		left.anchor = GridBagConstraints.EAST;
		GridBagConstraints right = new GridBagConstraints();
		right.weightx = 2.0;
		right.gridwidth = GridBagConstraints.REMAINDER;
		
		
		panel.add(new JLabel(specialEditor[1]), left);
		panel.add(columns, right);
		panel.add(new JLabel(specialEditor[2]), left);
		panel.add(rows, right);
		useWrap.setSelected(((TextArea) tempComponent).getWrap());
		panel.add(useWrap, right);
		panel.add(enter, left);
		panel.add(exit, right);
		
	
		window.add(panel);
		
		enter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((TextArea) tempComponent).setTextCols((Integer)columns.getValue());
				((TextArea) tempComponent).setTextRows((Integer)rows.getValue());
				((TextArea) tempComponent).setWrap((Boolean)useWrap.isSelected());
				window.dispose();
			}
		});
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.dispose();
			}
			
		});
		return window;
	}
/**
 * Initializes the layoutCode for button and sends all the variables in the string it sends. This includes the getCol,
 * getRow, getCols, getRows, getAnchor, getFill and variableName.
 * @return layoutCode which includes all the data needed to set the layoutCode.	
 */
	@Override
	public String getLayoutCode() {
		String layoutCode = "\t\tgbc.gridx = "+getCol()+ ";\n" + "\t\tgbc.gridy = "+getRow()+";\n"
                + "\t\tgbc.gridwidth = "+getCols()+";\n" + "\t\tgbc.gridheight = "+getRows()+";\n"
                + "\t\tgbc.anchor = "+getAnchor()+";\n" + "\t\tgbc.fill = "+getFill()+";\n"
                + "\t\tlayout.setConstraints(" + variableName + ", gbc);\n"
                + "\t\tadd(" +variableName +");\n" + "\t\t"+variableName+".setWrapStyleWord("+getWrap()+");\n";
		return layoutCode;
	}
}
