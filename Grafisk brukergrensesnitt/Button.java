package no.ntnu.imt3281.project1;

/**
 * This class handles all components of button. It also initializes the variables with the constructor button.
 * It sends out the definition and layoutCode for button.
 * @author Martin, Stian and Cim
 *
 */
public class Button extends BaseComponent {
	
	private static final long serialVersionUID = 7777;
/**
 * Constructor for Button BaseComponent. Which initializes the components.
 * @param component
 */
	public Button(BaseComponent component) {
		row = component.row;
		col = component.col;
		rows = component.rows;
		cols = component.cols;
		anchor = component.anchor;
		text = component.text;
		fill = component.fill;
		type = component.type;
		variableName = component.variableName;
	}
/**
 * Constructor for Button. Initializes the variables in button.
 */
	public Button() {
		row = 1;
		col = 1;
		rows = 1;
		cols = 1;
		anchor = 10;
		text = "";
		fill = 0;
		variableName = "component" + (nextComponentID -1);
		type = "JButton";
	}
/**
 * Initializes the definition of button component and creates a new JButton.
 * @return definition which includes the string given in the method.
 */
	@Override
	public String getDefinition() {
	    return "\tJButton "+variableName + " = new JButton(\""+text +"\");\n";
	}
/**
 * Initializes the layoutCode for button and sends all the variables in the string it sends. This includes the getCol,
 * getRow, getCols, getRows, getAnchor, getFill and variableName.
 * @return layoutCode which includes all the data needed to set the layoutCode.	
 */
	public String getLayoutCode() {

			String layoutCode = "\t\tgbc.gridx = "+getCol()+";\n" + "\t\tgbc.gridy = "+getRow()+";\n"
	                + "\t\tgbc.gridwidth = "+getCols()+";\n" + "\t\tgbc.gridheight = "+getRows()+";\n"
	                + "\t\tgbc.anchor = "+getAnchor()+";\n" + "\t\tgbc.fill = "+getFill()+";\n"
	                + "\t\tlayout.setConstraints(" + variableName + ", gbc);\n"
	                + "\t\tadd(" +variableName +");\n";
		return layoutCode;
	}

}
