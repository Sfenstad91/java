package no.ntnu.imt3281.project1;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import no.ntnu.imt3281.project1.constants;

/**
 * This Class contains everything about the GUI of the application. It contains the windows and the buttons and
 * the actions of the buttons. It has the SpecialEditor and toolbars and popupmenues and tooltips for the application.
 * It implements ActionListener to listen for button pushes or other actions throughout the use of the application.
 * @see java.awt.event.ActionListener
 * @author Martin, Stian and Cim
 *
 */
public class tableWithTableModel extends JFrame implements ActionListener{
	/**
	 * Creates a new GBLEDataModel, new JTable and a BaseComponent.
	 * Also adds the correct language to the different buttons and menues in the application
	 * and the specialEditor.
	 */
	private static final long serialVersionUID = 1L;
	GBLEDataModel gbled;
	JTable jtable;
	BaseComponent tempComponent;
	
	String[] menuItems = {I18n.getBundle().getString("menuItems.properties"),
						  I18n.getBundle().getString("menuItems.remove"),
						  I18n.getBundle().getString("menuItems.moveup"),
						  I18n.getBundle().getString("menuItems.movedown")};
	String[] topMenu = {I18n.getBundle().getString("topMenu.file"),
						I18n.getBundle().getString("topMenu.edit"),
						I18n.getBundle().getString("topMenu.help"),
						I18n.getBundle().getString("topMenu.clear"),
						I18n.getBundle().getString("topMenu.load"),
						I18n.getBundle().getString("topMenu.save"),
						I18n.getBundle().getString("topMenu.add")};
	/**
	 * Constructor for the tableWithTableModel Class, it initiates all the buttons and
	 * popupmenues in the application and contains the actionListener for the specialEditor.
	 * 
	 */
	public tableWithTableModel() {
		gbled = new GBLEDataModel();
		Label label = new Label();
		jtable = new JTable(gbled);
		
		JToolBar toolbar = new JToolBar();
		addButtons(toolbar);
		
		add( new JScrollPane(jtable));
		setSize(640, 480);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// For adding of menu
		createMenu();
		final JPopupMenu popupmenu = new JPopupMenu("Edit");
		JMenuItem properties = new JMenuItem(menuItems[0]);
		JMenuItem remove = new JMenuItem(menuItems[1]);
		JMenuItem moveup = new JMenuItem(menuItems[2]);
		JMenuItem movedown = new JMenuItem(menuItems[3]);
		
		
		popupmenu.add(properties);
		popupmenu.add(remove);
		popupmenu.add(moveup);
		popupmenu.add(movedown);
		/**
		 * Create a new editor for properties of either TextField or TextArea
		 * @see javax.swing.AbstractButton.addActionListener
		 */
		properties.addActionListener(new ActionListener() {
			/**
			 * ActionPerformed for event if user triggers the specialEditor in the application.
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog window;
				 tempComponent = gbled.getComponentAtIndex(jtable.getSelectedRow());
				 window = tempComponent.getSpecialEditor();
				 if(window != null){
					 window.setSize(320, 240);
					 window.setLocationRelativeTo(getContentPane());
					 window.setVisible(true);
				 }
			}
		});
		/**
		 *  If remove was clicked the action remove will start.
		 */
		remove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				label.setText("Remove is clicked");
				removeComp();
			}
		});
		
		moveup.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				label.setText("Move up was clicked");
				moveCompUp();
			}
		});
		
		movedown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				label.setText("Move down was clicked");
				moveCompDown();
			}
		});
		jtable.setComponentPopupMenu(popupmenu);	
	}
	/**
	 * Method that adds all the buttons to the tool bar, dropdown menues, listeners for action
	 * and adds the correct pictures to the buttons.
	 * @param toolbar 
	 */
	private void addButtons(JToolBar toolbar) {
		Container contentPane = getContentPane();
		contentPane.add(toolbar, BorderLayout.NORTH);
		//Buttons for editing table
		JComboBox comboBox = new JComboBox();
		JButton add;
		JButton up;
		JButton down;
		JButton save;
		JButton load;
		JButton clear;
		JButton gen;
		
		//Tooltip strings for buttons
		String addFile = I18n.getBundle().getString("toolbar.add");
		String moveUp = I18n.getBundle().getString("toolbar.moveUp");
		String moveDown = I18n.getBundle().getString("toolbar.moveDown");
		String saveFile = I18n.getBundle().getString("toolbar.save");
		String loadFile = I18n.getBundle().getString("toolbar.load");
		String clearFile = I18n.getBundle().getString("toolbar.clear");
		String genFile = I18n.getBundle().getString("toolbar.gen");
		
		//Add images to each button
		add = createButton("NewRow.GIF", addFile, "add");
		up = createButton("MoveRowUp.GIF", moveUp, "up");
		down = createButton("MoveRowDown.GIF", moveDown, "down");
		save = createButton("SAVE.GIF", saveFile, "save");
		load = createButton("OPENDOC.GIF", loadFile, "load");
		clear = createButton("ExecuteProject.GIF", clearFile, "clear");
		gen = createButton("SaveJava.GIF", genFile, "gen");
		
		//Create action listener to each button.
		add.addActionListener(this);
		up.addActionListener(this);
		down.addActionListener(this);
		save.addActionListener(this);
		load.addActionListener(this);
		clear.addActionListener(this);
		gen.addActionListener(this);
		
		//Add the buttons to the tool bar
		toolbar.add(add);
		toolbar.add(up);
		toolbar.add(down);
		toolbar.add(save);
		toolbar.add(load);
		toolbar.add(clear);
		toolbar.add(gen);
		
		//Creates a drop down menu for changing component type
		comboBox.addItem("JTextField");
		comboBox.addItem("JButton");
		comboBox.addItem("JTextArea");
		comboBox.addItem("JLabel");
		
		jtable.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(comboBox));

		JComboBox edi = new JComboBox();
	    JComboBox fillEdi = new JComboBox();
	    
	    // Fill our comboboxes with the values they sould have
	    for (int x =0; x<constants.fillPictures.length; x++)
	    	fillEdi.addItem(x);

	    for (int x =10; x<10+constants.anchorPictures.length; x++)
	    	edi.addItem(x);
	    
	    // The combobox have the possibility to have a custom render, so lets set it so it renders the number
	    // As the image we want.
	    edi.setRenderer(new DefaultListCellRenderer() {
	    	@Override
            public Icon getIcon ()
            {
                return new ImageIcon(constants.anchorPictures[(Integer.parseInt(getText()))-10]);
            }
	    
	    });
	    
	    fillEdi.setRenderer(new DefaultListCellRenderer() {
	    	@Override
            public Icon getIcon ()
            {
                return new ImageIcon(constants.fillPictures[(Integer.parseInt(getText()))]);
            }
	    
	    });
	    
	    TableColumn anchor = jtable.getColumnModel().getColumn(7);
	    TableColumn fill = jtable.getColumnModel().getColumn(8);

	    // Set the newly created combox as the cell editors
	    anchor.setCellEditor(new DefaultCellEditor(edi));
	    fill.setCellEditor(new DefaultCellEditor(fillEdi));

	    // The render we sat last time, was only for the combobox, when we use the dropdown
	    // So lets now do the same so we can render it when we just have selected something
	    // TODO - all the new imageIcon... is just code duplicates. 
	    // potensially ove to a function -  Icon getImage(string[] pictureList, ImageID);
	    anchor.setCellRenderer((new DefaultTableCellRenderer(){
	    	
	    	@Override
            public Icon getIcon ()
            {
                return new ImageIcon(constants.anchorPictures[(Integer.parseInt(getText()))-10]);
            }
	    }));
	    
fill.setCellRenderer((new DefaultTableCellRenderer(){
	    	
	    	@Override
            public Icon getIcon ()
            {
                return new ImageIcon(constants.fillPictures[(Integer.parseInt(getText()))]);
            }
	    }));
		
	
	}
	/**
	 * Method Action performed
	 * Calls the intended methods invoked by the action event
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		case "add":
			addingComponent();
			break;
		case "up":
			moveCompUp();
			break;
		case "down":
			moveCompDown();
			break;
		case "save":
			saveComp();
			break;
		case "load":
			loadComp();
			break;
		case "clear":
			clearComp();
			break;
		case "gen":
			genCode();
			break;
		default:
			return;
		}

	}
	/**
	 * Method to create button with correct picture and tooltips.
	 * @param image
	 * @param tooltip
	 * @param value
	 * @return button which contains the correct pictures, tooltips and the actions sent.
	 */
	private JButton createButton(String image, String tooltip, String value) {
		JButton button = new JButton(new ImageIcon("pictures/graphics/" + image));
		button.setToolTipText(tooltip);
		button.setActionCommand(value);
		return button;
	}
	/**
	 * Method to clear components. Uses the GBLEDataModel and the clear() method.
	 * @see no.ntnu.imt3281.project1.GBLEDataModel#clear()
	 * @see no.ntnu.imt3281.project1.tableWithTableModel#remake()
	 */
	private void clearComp() { // Button for clearing a component.
		gbled.clear();
		remake();
	}
	/**
	 * Method to load from filesystem and add to the table of contents.
	 * Uses JFileChooser and ByteArrayStream. It takes the file as bytes and changes it to
	 * string later on and adds it to the table. It also handles if the user uses cancel.
	 * @see javax.swing.JFileChooser
	 * @see javax.swing.JFileChooser#showOpenDialog(Component)
	 */
	private void loadComp() {
		JFileChooser chooser = new JFileChooser();
		int result = chooser.showOpenDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION) {
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		String path=chooser.getSelectedFile().getAbsolutePath();
		
		try {
			byte[] b = Files.readAllBytes(Paths.get(path));
			ByteArrayInputStream bis = new ByteArrayInputStream(b);
			gbled.load(bis);
			remake();
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		} else if (result == JFileChooser.CANCEL_OPTION) {
			System.out.println("Cancel was pressed");
		}
	}
	/**
	 * Method to save the content of table to a file. It saves it as bytes and to the
	 * chosen location with the chosen name of the user. It uses JFileChooser to open
	 * the navigation for saving and to handle actions.
	 * @see javax.swing.JFileChooser
	 * @see javax.swing.JFileChooser#showSaveDialog(Component)
	 */
	private void saveComp() {
		JFileChooser chooser = new JFileChooser();
		int result = chooser.showSaveDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION) {
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		String path=chooser.getSelectedFile().getAbsolutePath();
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
	
		gbled.save(bos);
		OutputStream outputStream;
		try {
			outputStream = new FileOutputStream(path);
				try {
					bos.writeTo(outputStream);
					remake();
				} catch (IOException e) {
					e.printStackTrace();
				}
				outputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		} else if (result == JFileChooser.CANCEL_OPTION) {
			System.out.println("Cancel was pressed");
		}
	
    }
	/**
	 * This method is to remove a selected row from the table.
	 */
	private void removeComp() {
		//Add if there is no component. do nothing
		gbled.removeComponent(jtable.getSelectedRow());
		remake();
	}
	/**
	 * This method is to move the chosen line one spot down as long as it can.
	 */
	private void moveCompDown() {
		if(!(jtable.getSelectedRow() < 0 || jtable.getSelectedRow() >= gbled.getColumnCount())) {
			gbled.moveComponentDown(jtable.getSelectedRow());
			remake();
		}
	}
	/**
	 * Method that moves a component up one row in JTable.
	 */
	private void moveCompUp() {
		if(!(jtable.getSelectedRow() <= 0 || jtable.getSelectedRow() > gbled.getColumnCount())) {
			gbled.moveComponentUp(jtable.getSelectedRow());
			remake();
		}
	}
	/**
	 * Method that adds a component to the JTable.
	 */
	private void addingComponent() {
		gbled.addComponent(new Label());
		remake();
	}
	/**
	 * Method for repainting the JTable, needed after almost every change.
	 */
	private void remake() {
		jtable.revalidate();
		jtable.repaint();
	}
	
	/**
	 * Method to generate javaCode to file. This method will take whatever is in the table at the moment
	 * and generate a javaCode from it. It used getLayoutCode and getDefinitions with the gbled object.
	 * It uses printWriter to write to the file, and it saves to a .java file.
	 * @see no.ntnu.imt3281.project1.GBLEDataModel#getDefinitions()
	 * @see no.ntnu.imt3281.project1.GBLEDataModel#getLayoutCode()
	 * @see javax.swing.JFileChooser
	 * @see java.lang.StringBuilder
	 * @see java.io.PrintWriter
	 */
	public void genCode() {
		JFileChooser chooser = new JFileChooser();
		int result = chooser.showSaveDialog(this);
		StringBuilder content = new StringBuilder();
		
			if (result == JFileChooser.APPROVE_OPTION) {
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);		
			String fileName=chooser.getSelectedFile().getName();
			String path = chooser.getSelectedFile().getPath();
			String defs = gbled.getDefinitions();	    
	        String layout = gbled.getLayoutCode();
			try {
				PrintWriter out = new PrintWriter(path+ ".java");
				content.append(" Import Javax.swing.*;\r\n" +
	        			  "import java.awt.*;\r\n" +
	        			  "\r\n" +
	        			  "/**\r\n" + 
	        			  " * Code generated from GridBagLayoutEditor v 0.1\r\n" + 
	        			  " */\r\n" + 
	        			  "public class " + fileName + " extends JPanel {"
	        			  + "\r\n");
		        content.append(defs);
		        content.append("\n" + 
							   "  public " + fileName +" () {\r\n" + 
							   "    GridBagLayout layout = new GridBagLayout ();\r\n" + 
							   "    GridBagConstraints gbc = new GridBagConstraints();\r\n" + 
							   "    setLayout (layout);"
							   + "\r\n");
		        content.append(layout);
		        content.append("  }\r\n" + 
							   "}\r\n" + 
							   "\r\n");
		        out.println(content);
		        out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		   
			} else if (result == JFileChooser.CANCEL_OPTION) {
				System.out.println("Cancel was pressed");
			}
	}
	
	/**
	 * This method creates a menu containing file(with submenues), edit(with submenu) and help (which is not implemented).
	 * It uses JMenuBar, JMenu and JMenuItem and it sends the action to the other methods in this class.
	 * @see javax.swing.JMenuBar
	 * @see javax.swing.JMenu
	 * @see javax.swing.JMenuItem
	 */
	public void createMenu() {
		JMenuBar menuBar;
		JMenu menu;
		menuBar = new JMenuBar();
		
		// Menu for file
		menu = new JMenu(topMenu[0]);
		menu.setMnemonic(KeyEvent.VK_A);
		menuBar.add(menu);
		
		JMenuItem clear = new JMenuItem(topMenu[3], new ImageIcon("pictures/graphics/ExecuteProject.gif"));
		clear.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));
		menu.add(clear);
		
		clear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearComp();
			}
		});
		
		JMenuItem load = new JMenuItem(topMenu[4], new ImageIcon("pictures/graphics/OpenDoc.gif"));
		load.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));
		menu.add(load);
		
		load.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadComp();
			}
		});
		
		JMenuItem save = new JMenuItem(topMenu[5], new ImageIcon("pictures/graphics/Save.gif"));
		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.ALT_MASK));
		menu.add(save);
		
		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveComp();
			}
		});
		
		// Menu for Edit
		menu = new JMenu(topMenu[1]);
		menu.setMnemonic(KeyEvent.VK_A);
		menuBar.add(menu);
		
		JMenuItem add = new JMenuItem(topMenu[6], new ImageIcon("pictures/graphics/NewRow.gif"));
		add.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));
		menu.add(add);
		
		add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addingComponent();
			}
		});
		
		// Menu for Help
		menu = new JMenu(topMenu[2]);
		menu.setMnemonic(KeyEvent.VK_A);
		menuBar.add(menu);
		
		setJMenuBar(menuBar);
		
		
	}
	
}
