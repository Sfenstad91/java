package no.ntnu.imt3281.project1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Vector;
/**
 * @author Martin, Stian and Cim
 *
 * The class GBLEDataModel provides data for the
 * JTable which acts as the view of our application. The model can be
 * saved to and loaded from a file and the definitions and layout code
 * for all components can be extracted as separate strings. It extends from AbstractTableModel.
 * @see javax.swing.table.AbstractTableModel
 */
public class GBLEDataModel extends javax.swing.table.AbstractTableModel {
	
	private static final long serialVersionUID = 7777;
	/**
	 * This Vector contains all he data from baseComponent, and is needed for us to have access to the components.
	 * The array String ColumnNames, works as a header for the applications, and is internationalized.
	 * @see no.ntnu.imt3281.project1.I18n#getBundle()
	 * @see java.util.ResourceBundle#getString(String)
	 */
	Vector<BaseComponent> data = new Vector<>();
	String[] columnNames ={ I18n.getBundle().getString("GBLEDataModel.type"),
							I18n.getBundle().getString("GBLEDataModel.name"),
							I18n.getBundle().getString("GBLEDataModel.text"),
							I18n.getBundle().getString("GBLEDataModel.row"),
							I18n.getBundle().getString("GBLEDataModel.column"),
							I18n.getBundle().getString("GBLEDataModel.rows"),
							I18n.getBundle().getString("GBLEDataModel.columns"),
							I18n.getBundle().getString("GBLEDataModel.anchor"),
							I18n.getBundle().getString("GBLEDataModel.fill")};
	
	/**
	 * This method collects the content of columnNames which works as a header for the application.
	 * It stores Component Type, Variable Name, Text, Row, Column, Rows, Columns, Anchor and Fill
	 * in different languages. This is a method Override from AbstractTableModel.
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 * @param id which is the placement of the different headers in columnName.
	 * @return columnNames[id] that write out the stored values in columnNames with id as help.
	 */
	@Override
	public String getColumnName(int id) { // This is to make the header including the names of the columns
		return columnNames[id];
	}
	/**
	 * Method that finds the numbers of rows. This method uses the vector data from BaseComponent and 
	 * the size() method to return the number of components in the data vector.
	 * This is a method Override from AbstractTableModel
	 * @see javax.swing.table.AbstractTableModel#getRowCount()
	 * @see java.util.Vector#size()
	 * @return data.size() which includes the number of components in the vector.
	 */
	@Override
	public int getRowCount() {
		return data.size();
	}
	/**
	 * Method that returns the number of columns in the model. As a default it returns 9, because we want
	 * a total of 9 columns for this application. This is a method Override from AbstractTableModel
	 * @see javax.swing.table.AbstractTableModel#getColumnCount()
	 * @return 9 which is the number of columns we want for this application.
	 */
	@Override
	public int getColumnCount() {
		return 9;
	}
	/**
	 * Takes two parameters and returns the correct get method from a switch case.
	 * Works together with setValueAt
	 * @see no.ntnu.imt3281.project1.GBLEDataModel#setValueAt(Object, int, int)
	 * @param rowIndex which is a integer containing the row.
	 * @param columnIndex which is a integer containing the column.
	 * @return the correct method due to switch.
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		BaseComponent bc; // Makes a new BaseComponent
		bc = data.elementAt(rowIndex); // Adds the element at rowIndex from the vector and stores in the new BaseComponent.
		
		switch(columnIndex) {
			case 0:
				return bc.getType();
			case 1:
				return bc.getVariableName();
			case 2:
				return bc.getText();
			case 3:
				return bc.getRow();
			case 4:
				return bc.getCol();
			case 5:
				return bc.getRows();
			case 6:
				return bc.getCols();
			case 7:
				return bc.getAnchor();
			case 8:
				return bc.getFill();
			default:
				return 0;
			}
	}
	/**
	 * Method that sets the values in the different set methods. Does this by taking in three parameters,
	 * and sending the value with the help of rowIndex and ColumnIndex and a switch.
	 * This method works together with getValueAt
	 * @see no.ntnu.imt3281.project1.GBLEDataModel#getValueAt(int, int)
	 * @param value contains the value set by the user, or the predetermined value set my initialization
	 * @param rowIndex which is a integer containing a row
	 * @param columnIndex which is a integer containing a column
	 * @return if something didn't go as planned, does not return if setValue went thru
	 */
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		
		BaseComponent bc;
		bc = data.elementAt(rowIndex);
		
		switch(columnIndex) {
		case 0:
			BaseComponent newObj;
			if (value == "JLabel") {
				bc.setType(value.toString());
				newObj = new Label(bc);
			} else if (value == "JTextField") {
				bc.setType(value.toString());
				newObj = new TextField(bc);
			} else if (value == "JTextArea") {
				bc.setType(value.toString());
				newObj = new TextArea(bc);
			} else if (value == "JButton") {
				bc.setType(value.toString());
				newObj = new Button(bc);
			} else {
				System.out.println("Error in setValueAt");
				return;
			}
			data.add(rowIndex, newObj);
			data.remove(rowIndex + 1);
			return;
		case 1:
			bc.setVariableName((String) value);
			break;
		case 2:
			bc.setText((String) value);
			break;
		case 3:
			bc.setRow(Integer.parseInt((String)value));
			break;
		case 4:
			bc.setCol(Integer.parseInt((String)value));
			break;
		case 5:
			bc.setRows(Integer.parseInt((String)value));
			break;
		case 6:
			bc.setCols(Integer.parseInt((String)value));
			break;
		case 7:
			bc.setAnchor((int)value);
			break;
		case 8:
			bc.setFill((int)value);
			break;
		default:
			return;
		}
	}
	/**
	 * To add a new component to vector data. With the help of the vector data
	 * and the add() method it adds a new component to data.
	 * @see java.util.Vector#add(Object)
	 * @param component receives a component to add to data
	 */
	public void addComponent(BaseComponent component) {
		data.add(component);
	}
	/**
	 * To remove a component from vector data. Takes a integer as parameter and
	 * removes the chosen component, it checks the row count and uses a if statement
	 * to find if the parameter is within its limits and if so it can remove the component.
	 * @see no.ntnu.imt3281.project1.GBLEDataModel#getRowCount()
	 * @see java.util.Vector#remove(int)
	 * @param i is a chosen component that will be removed as long as it within the if statement.
	 */
	public void removeComponent(int i) {
		int row = getRowCount();
		if(!(i < 0 || i > row))
			data.remove(i);
	}
	/**
	 * To remove a component from vector data. Takes a object as parameter and
	 * removes the chosen component with the help of remove() and the vector data.
	 * @see java.util.Vector#remove(Object)
	 * @param component is a chosen component that will be removed as long as it within the if statement.
	 */
	public void removeComponent(BaseComponent component) {
		data.remove(component);
	}
	/**
	 * To move a component up one spot. Takes a integer parameter which is the chosen line to move.
	 * Goes thru a if statement to check if the line is at the top and cannot be move further up.
	 * It takes the parameter given and swaps it with the line above and presses the line above one spot down.
	 * @see java.util.Collections#swap(java.util.List, int, int)
	 * @see no.ntnu.imt3281.project1.GBLEDataModel#getColumnCount()
	 * @param i the chosen line to move up
	 */
	public void moveComponentUp(int i) {
		if (i-1 >= 0 && i < getRowCount())
			Collections.swap(data, i, i-1);
	}
	/**
	 * Method to move a component down one spot. Takes a integer parameter which is the chosen line to move.
	 * goes thru a if statement to check if the line is at the bottom and cannot be moved further down.
	 * It takes the parameter given and swaps it with the line below and pushes the line below one spot up.
	 * @see java.util.Collections#swap(java.util.List, int, int)
	 * @see no.ntnu.imt3281.project1.GBLEDataModel#getColumnCount()
	 * @see java.util.Vector#size()
	 * @param i the chosen line to move down
	 */
	public void moveComponentDown(int i) {
		if(!((i < 0 || i >= getColumnCount()) || i + 1 >= data.size())) {
			Collections.swap(data, i, ++i);
		}
	}
	/**
	 * Method to save the content of the table (all lines and changes made) to a file.
	 * It creates a new ObjectOutput object and stores the parameter bos in it. Bos is a ByteArrayOutputStream
	 * and contains bytes. The method checks thru the whole of data and writes what is in tmp to the new object out.
	 * @see java.io.ByteArrayOutputStream
	 * @see java.io.ObjectOutput#writeObject(Object)
	 * @param bos contains the content as bytes.
	 */
	public void save(ByteArrayOutputStream bos) {
		try {
			ObjectOutput out = new ObjectOutputStream(bos);
			for(BaseComponent tmp : data) {
				out.writeObject(tmp);
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	/**
	 * Method to load a saved file to the table. This program can get a file from your computer and show
	 * it in the table as it was saved. It gets the file in bytes and changes it to string later on.
	 * It creates a new ObjectInputStream object and stores the data in bis.		
	 * @see java.io.ByteArrayInputStream
	 * @see java.io.ObjectInput#readObject()
	 * @see java.io.ObjectInput#available()
	 * @param bis contains the content as bytes
	 */
	public void load(ByteArrayInputStream bis) {
		ObjectInput in;
		try {
			in = new ObjectInputStream(bis);
		
			while(bis.available() > 0) {
				try {
					data.add((BaseComponent) in.readObject());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	/**
	 * Method to clear the full table. This just takes the vector data containing all the components and clears it.
	 * @see java.util.Vector#clear()
	 */
	public void clear() {
		data.clear();
	}
	/**
	 * Collects all the definitions from the different classes and saves out the correct one.
	 * Creates a String retString and goes thru the data vector and saves the definition it finds in the String
	 * and returns it.
	 * @return the definition from a correct class.
	 */
	public String getDefinitions() {
		String retString = "";
		for(BaseComponent comp : data) {
			retString += comp.getDefinition();
		}
		return retString;
	}
	/**
	 * Collects all the layoutCodes from the different classes and saves the correct one.
	 * Creates a String retString and goes thru the data vector and saves the layoutCode it finds in the String
	 * and returns it.
	 * @return the layoutcode from the correct class.
	 */
	public String getLayoutCode() {
		String retString = "";
		for(BaseComponent comp : data) {
			retString += comp.getLayoutCode();
		}
		return retString;
	}
	/**
	 * Checks if is cell editable, and returns true as default.
	 * @return true as default
	 */
	@Override
	public boolean isCellEditable(int row, int col) {
		return true;
    }
	/**
	 * Takes a integer parameter and checks the element at the given index and returns it.
	 * @param index
	 * @return the component at the specified index thru the parameter index.
	 */
	public BaseComponent getComponentAtIndex(int index) {
		return data.elementAt(index);
	}

}
