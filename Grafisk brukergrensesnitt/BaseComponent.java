package no.ntnu.imt3281.project1;

import java.io.Serializable;

import javax.swing.JDialog;

/**
 * This class handles all components from TextField, TextArea, Button and Label. It implements @see Serializable.
 * It consists of four constructors, and 21 methods.
 * 
 * @author Martin, Stian and Cim
 *
 */
public class BaseComponent implements Serializable{

	
		String variableName;
		String text;
		String type;
		int row;
		int col;
		int rows;
		int cols;
		int anchor;
		int fill;
		static int nextComponentID;
		
		private static final long serialVersionUID = 7777;
		String[] buttons = {I18n.getBundle().getString("editorButton.enter"),
							I18n.getBundle().getString("editorButton.exit")};
		String[] specialEditor = {I18n.getBundle().getString("specialEditor.width"),
				  				  I18n.getBundle().getString("specialEditor.textColumns"),
				  				  I18n.getBundle().getString("specialEditor.textRows"),
				  				  I18n.getBundle().getString("specialEditor.wordWrap")};

		/**
		 * BaseComponent component constructor. Collects all data of components for BaseComponent.
		 * @param component from baseComponent.
		 */
		public BaseComponent(BaseComponent component) {
			row = component.row;
			col = component.col;
			rows = component.rows;
			cols = component.cols;
			anchor = component.anchor;
			text = component.text;
			fill = component.fill;
			variableName = component.variableName;
			type = component.type;
		}
		/**
		 * BaseComponent constructor initializes all data about BaseComponent. Adds +1 to componentID
		 * when new component is added.
		 */
		public BaseComponent() {
			nextComponentID++;
			row = 1;
			col = 1;
			rows = 1;
			cols = 1;
			anchor = 10;
			text = "";
			fill = 0;
			variableName = "component" + (nextComponentID -1);
			type = "JLabel";
		}
		/**
		 * getVariableName gets variableName from setVariableName and returns said name.
		 * @return variableName which consists of the name of the component.
		 */
		public String getVariableName() {			
			return variableName;
		}
		/**
		 * Collects what type the component should be. JTextArea, JLabel, JButton or JTextFiel.
		 * @param string from type of each class, depending on what class is called
		 */
		public void setType(String string) {
			type = string;
		}
		/**
		 * Collects given type from setType method.
		 * @return type set by setType.
		 */
		public String getType() {
			return type;
		}
		/**
		 * Collects Text given from setText method. Text is filled in by user.
		 * @return text
		 */
		public String getText() {
			return text;
		}
		/**
		 * Collects Row from setRow method.
		 * @return row
		 */
		public int getRow() {
			return row;
		}
		/**
		 * Collects column from setCol method.
		 * @return col
		 */
		public int getCol() {
			return col;
		}
		/**
		 * Collects data from setCols method.
		 * @return cols
		 */
		public int getCols() {
			return cols;
		}
		/**
		 * Collects Rows from setRows method.
		 * @return rows
		 */
		public int getRows() {
			return rows;
		}
		/**
		 * Collects anchor from setAnchor method.
		 * @return anchor
		 */
		public int getAnchor() {
			return anchor;
		}
		/**
		 * Collects fill from setFill method.
		 * @return fill
		 */
		public int getFill() {
			return fill;
		}
		/**
		 * Set a predetermined text or takes a text change from 
		 * user and changes what is in variable text.
		 * @param string consisting of text given from user.
		 */
		public void setText(String string) {
			text = string;
		}
		/**
		 * Set a predetermined text or takes a text change from
		 * user and changes what is in the variable variableName.
		 * @param string consisting of name given by user.
		 */
		public void setVariableName(String string) {
			variableName = string;
		}
		/**
		 * Sets a predetermined column value and can take changes from user,
		 * which decides what is in the variable col.
		 * @param i integer 1 or from user.
		 */
		public void setCol(int i) {
			col = i;
		}
		/**
		 * Sets a predetermined row value and can take changes from user,
		 * which decides what is in the variable row.
		 * @param i integer 1 or from user.
		 */
		public void setRow(int i) {
			row = i;
		}
		/**
		 * Sets a predetermined columns value and can take changes from user,
		 * which decides what is in the variable cols.
		 * @param i integer 1 or from user.
		 */
		public void setCols(int i) {
			cols = i;
		}
		/**
		 * Set a predetermined rows value and can take changes from user,
		 * which decides what is in the variable rows.
		 * @param i integer 1 or from user.
		 */
		public void setRows(int i) {
			rows = i;
		}
		/**
		 * Set a predetermined anchor value and can take changes from user,
		 * which decides what picture to show in the anchor frame of GUI
		 * @param i integer 10 or a change from user connected to different pictures.
		 */
		public void setAnchor(int i) {
			anchor = i;
		}
		/**
		 * Set a predetermined fill value and can take changes from user,
		 * which decides what picture to show in the fill frame of GUI
		 * @param i integer 0 or a change from user connected to different pictures.
		 */
		public void setFill(int i) {
			fill = i;
		}
		/**
		 * Layoutcode builds the layoutcode including the given column, columns,
		 * row, rows, anchor, fill and variablename
		 * @return layoutCode including the different given variables.
		 */
		public String getLayoutCode() {
			String layoutCode = "\t\tgbc.gridx = "+getCol()+";\n" + "\t\tgbc.gridy = "+getRow()+";\n"
	                + "\t\tgbc.gridwidth = "+getCols()+";\n" + "\t\tgbc.gridheight = "+getRows()+";\n"
	                + "\t\tgbc.anchor = "+getAnchor()+";\n" + "\t\tgbc.fill = "+getFill()+";\n"
	                + "\t\tlayout.setConstraints(" + variableName + ", gbc);\n"
	                + "\t\tadd(" +variableName +");\n";
			return layoutCode;
		}
		/**
		 * 
		 * @return null
		 */
		public String getDefinition() {
			return null;
		}
		/**
		 * 
		 * @return null
		 */
		public JDialog getSpecialEditor() {
			return null;
		}
}
