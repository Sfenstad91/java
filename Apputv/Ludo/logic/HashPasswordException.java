package no.ntnu.imt3281.ludo.logic;

/**
 * HashPasswordException
 */
public class HashPasswordException extends RuntimeException {
	/**
	 * No room for player exception
	 */
	public HashPasswordException() {
		super();
	}

	/**
	 * No room for more playersException.
	 * @param message
	 */
	public HashPasswordException(String message) {
		super(message);
	}

	/**
	 * HashPasswordException
	 * @param message
	 * @param cause
	 */
	public HashPasswordException(String message, Throwable cause) {
		super(message, cause); 
	}

	/**
	 * HashPasswordException
	 * @param cause
	 */
	public HashPasswordException(Throwable cause) {
		super(cause);
	}
}
