package no.ntnu.imt3281.ludo.logic;

/**
 * No room for more players exception
 * Should be raised when the game is full.
 */
public class NoRoomForMorePlayersException extends RuntimeException {
	/**
	 * No room for player exception
	 */
	public NoRoomForMorePlayersException() {
		super();
	}

	/**
	 * No room for more playersException.
	 * @param message
	 */
	public NoRoomForMorePlayersException(String message) {
		super(message);
	}
}
