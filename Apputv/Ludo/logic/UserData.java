package no.ntnu.imt3281.ludo.logic;

/**
 * Userdata class.
 * Contains info about the logged inn user.
 */
public class UserData{
    private boolean isLoggedIn = false;
    private String token = null;
    private String userName = null;
    private int ID = -1;

    /**
     * Set the userID.
     * @param ID
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * Set the Logged in status.
     * @param loggedIn
     */
    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    /**
     * Set the userToken.
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Set the username.
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Get the UserID.
     * @return
     */
    public int getID() {
        return ID;
    }

    /**
     * Get the userToken.
     * @return
     */
    public String getToken() {
        return token;
    }

    /**
     * Get the username.
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Get the logged in status.
     * @return
     */
    public boolean getLoggedIn() {
        return isLoggedIn;
    }
}