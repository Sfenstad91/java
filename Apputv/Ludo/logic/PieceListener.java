package no.ntnu.imt3281.ludo.logic;

/**
 * PieceListner - used when listning to events about pieces being moved from the ludo game.
 */
public interface PieceListener {

	/**
	 * Handle the piece moved event.
	 * @param pe
	 */
	 void pieceMoved(PieceEvent pe);
}
