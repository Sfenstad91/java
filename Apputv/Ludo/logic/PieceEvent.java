package no.ntnu.imt3281.ludo.logic;

/**
 * Class pieceEvent. Used when players in the ludo game, move.
 */
public class PieceEvent {
	Integer player;
	Integer piece;
	Integer from;
	Integer to;

	/**
	 * Throw a new pieceEvent to all listners.
	 * @param ludo
	 * @param color
	 * @param piece
	 * @param from
	 * @param to
	 */
	public PieceEvent(Ludo ludo, int color, int piece, int from, int to) {
		setFrom(from);
		setPiece(piece);
		setTo(to);
		setPlayer(color);
		for (PieceListener hl : ludo.pieceListener)
			hl.pieceMoved(this);
	}

	/**
	 * Get the player from the event.
	 * @return the player in question. Their Color.
	 */
	public Integer getPlayer() {
		return player;
	}

	/**
	 * Sets the player.
	 * @param ludoPlayer
	 */
	public void setPlayer(Integer ludoPlayer) {
		player = ludoPlayer;
	}

	/**
	 * Get piece from the event.
	 * @return piece
	 */
	public Integer getPiece() {
		return piece;
	}

	/**
	 * setPiece to the event.
	 * @param p
	 */
	public void setPiece(Integer p) {
		piece = p;
	}

	/**
	 * getter. Gets where the player moved from.
	 * @return where the player moved from.
	 */
	public Integer getFrom() {
		return from;
	}

	/**
	 * Set from. Sets the from variable.
	 * @param frm
	 */
	public void setFrom(Integer frm) {
		from = frm;
	}

	/**
	 * Get the to variable.
	 * @return to - where the player is moving to.
	 */
	public Integer getTo() {
		return to;
	}

	/**
	 * Set to. Set where the player is moving to.
	 * @param t
	 */
	public void setTo(Integer t) {
		to = t;
	}


}
