package no.ntnu.imt3281.ludo.logic;

import java.util.Map;

/**
 * GameListner. Used when subscribing to events from the server.
 */
public interface GameListener {

	/**
	 * Something changed.
	 * @param msg
	 */
	 void GameChange(Map<String, Object> msg);
}
