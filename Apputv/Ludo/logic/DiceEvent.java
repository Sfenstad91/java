package no.ntnu.imt3281.ludo.logic;

/**
 * DiceEvent. Used for when a dice has been thrown.
 */
public class DiceEvent {
	Integer player;
	Integer dice;

	/**
	 * Tell all the listners that a dice have been thrown.
	 * @param ludo
	 * @param color
	 * @param number
	 */
	public DiceEvent(Ludo ludo, Integer color, int number) {
		setPlayer(color);
		setDice(number);
		for (DiceListener hl : ludo.diceListeners)
			hl.diceThrown(this);
	}

	/**
	 * Get the player in question.
	 * @return players color.
	 */
	public Integer getPlayer() {
		return player;
	}

	/**
	 * Set the player in question.
	 * @param playah
	 */
	public void setPlayer(Integer playah) {
		player = playah;
	}

	/**
	 * Get the dice value.
	 * @return dicevalue
	 */
	public Integer getDice() {
		return dice;
	}

	/**
	 * Set the dice value.
	 * @param doice
	 */
	public void setDice(Integer doice) {
		dice = doice;
	}
}
