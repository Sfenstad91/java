package no.ntnu.imt3281.ludo.logic;

/**
 * Not Enough player exception
 */
public class NotEnoughPlayersException extends RuntimeException {
	/**
	 * Not Enough player exception
	 */
	public NotEnoughPlayersException() {
		super();
	}

	/**
	 * Not Enough player exception
	 * @param message
	 */
	public NotEnoughPlayersException(String message) {
		super(message);
	}
	
}
