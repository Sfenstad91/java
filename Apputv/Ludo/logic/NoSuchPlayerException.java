package no.ntnu.imt3281.ludo.logic;

/**
 * NoSuckPlayerException. Not used.
 */
public class NoSuchPlayerException extends Exception {
	/**
	 * NoSuckPlayerException
	 */
	public NoSuchPlayerException() {
		super();
	}

	/**
	 * NoSuckPlayerException
	 * @param message
	 */
	public NoSuchPlayerException(String message) {
		super(message);
	}
}
