package no.ntnu.imt3281.ludo.logic;

import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.client.Globals;

import java.lang.reflect.Array;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * Ludo represents a game. each game has its own identificator. with four constants, RED, BLUE, YELLOW AND GREEN ranging from ints 0 to 3.
 * These colours represeents each player allowed in a single game.
 *Each player has a name, represented in the string saved in vector
 */

public class Ludo {
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);

	/** Color red */
	public static final Integer RED = 0;
	/** Color BLUE */
	public static final Integer BLUE = 1;
	/** Color Yellow */
	public static final Integer YELLOW = 2;
	/** Color Green */
	public static final Integer GREEN = 3;


	Vector<String> players = new Vector<>();
	Integer activePlayer;
	Integer dice;
	Random randomGenerator;
	int playerPieces[][];
	int lastDiceValue = 0;

	/**
	 * 	List of banned Pieces for this throw.
	 */
	Vector banlist;

	private String id = "-1";
	int[][] userGridToPlayerGrid;

	//TODO - more initialisation to constructor..
	Vector<DiceListener> diceListeners = new Vector<>();
	Vector<PieceListener> pieceListener = new Vector<>();
	Vector<PlayerListener> playerListener = new Vector<>();
	Vector Tower = new Vector(0, 1);
	static final String inactive = "Inactive: ";
	Integer throwCount = 0;
	String status = "";
	boolean threwAnSix = false;
	int winner = -1;

	/**
	 * Setup a ludo empty ludo game.
	 */
	public Ludo() {
		playerPieces = new int[4][];
		activePlayer = 0;
		setStatus("Created");
		initialiseUserGridToPlayerGrid();
	}

	private void setStatus(String newStatus) {
		status = newStatus;
	}

	/**
	 * Setup a ludoGame with 1 to 4 players.
	 * @param player1
	 * @param player2
	 * @param player3
	 * @param player4
	 */
	public Ludo(String player1, String player2, String player3, String player4){
		playerPieces = new int[4][];
		activePlayer = 0;
		initialiseUserGridToPlayerGrid();
		addPlayer(player1);
		addPlayer(player2);
		addPlayer(player3);
		addPlayer(player4);



		if(players.size() < 2) {
			throw new NotEnoughPlayersException();
		}

	}

	/**
	 * Initialises the UserGridToPlayerGrid Array.
	 * Using loops instead of the previus hardcoding.
	 */
	private void initialiseUserGridToPlayerGrid(){
		userGridToPlayerGrid = new int[60][4];

		int uof = 4; // 4 startFields per player
		int offset = 13; // Between nr.1 for each player
		int i = 0;
		// The first startField
		for(i = 0; i <1; i++){
			for(int playerID =0; playerID<4; playerID++)
				userGridToPlayerGrid[i][playerID]=i + (uof*playerID);
		}

		// Midgame fields
		// TODO - Should the first field be 16 or 17. On the colored field or not?
		// I starts on one
		for(; i <54; i++){
			for(int playerID =0; playerID<4; playerID++)
				userGridToPlayerGrid[i][playerID]=((i-1 + (offset*playerID))%52)+16;
		}

		// Final Line
		uof = 6; // The amount of tiles on the final line
		for(int d = 0; d <6; d++){
			int index = 54 +d; // The next in our list is 53, however the value we are at is:
			int value = d + 68; // Plus 68 because 16 startfields, and 52 midgame fields
			for(int playerID =0; playerID<4; playerID++)
				userGridToPlayerGrid[index][playerID]=value + (uof*playerID);
		}

	}

	/**
	 * Add a new player to the game.
	 * @param player name.
	 */
	public void addPlayer(String player){
		if(player != null) {
			if(players.size() == 4) {
				throw new NoRoomForMorePlayersException();
			}
			/** Initialize the piece array:*/
			playerPieces[players.size()] = new int[]{0,0,0,0};
			players.add(player);
			setStatus("Initiated");
		}
	}

	/**
	 * Remove a player from the game. In case he ends up being inactive.
	 * @param player
	 */
	public void removePlayer(String player) {
		Integer temp = players.indexOf(player);
		if(temp >= 0) {
			players.add(temp, inactive + player);
			players.remove(temp + 1);
		}

	}

	/**
	 * Return the nr of players.
	 * @return
	 */
	public float nrOfPlayers() {
		return players.size();
	}

	/**
	 * Get the amount of players, who is active.
	 * @return the amount of active players.
	 */
	public Integer activePlayers() {
		Integer count = 0;
		for(int i = 0; i < players.size(); i++) {
			if(players.elementAt(i).contains("Inactive")) {
				count++;
			}
		}
		return (players.size() - count);
	}

	/**
	 * Get the color of the active player
	 * @return color of active player.
	 */
	public Integer activePlayer() {
		return activePlayer;
	}

	/**
	 * Get the name of a player, given playerID
	 * @param playerId
	 * @return String username, or NULL
	 */
	public String getPlayerName(Integer playerId) {
		try {
			return players.get(playerId);
		}catch (IndexOutOfBoundsException e) {
			logger.throwing(this.getClass().getName(), "getPlayerName", e);
			return null;
		}
	}

	/**
	 * Get the position of a players piece.
	 * @param player
	 * @param piece
	 * @return position.
	 */
	public int getPosition(int player, int piece) {
		if(player < players.size())
			return playerPieces[player][piece];
		return 0;
	}
/**
 * Method to generate a random value on a dice thrown by a player. The value will be between 1 and 6.
 * @return the random value created by the randomGenerator which will be the dice value between 1 and 6.
 */
	public Integer throwDice() {

		randomGenerator = new Random();
		int diceSides = 6; // Number of faces on the dice.
		
		int result = randomGenerator.nextInt(diceSides); // Generates a random number between 0 and 5.
		result++; // Makes sure we stay inside the wanted range of 1 and 6.
		return throwDice(result);

	}

	/**
	 * Handles a users dicethrow.
	 *
	 * First a user can have 3 throw attempts to get a 6 if they dont have any active pieces.
	 * Here we define activePieces as pieces not at start and not in finish.
	 *
	 * next we have to check if a player, with activepieces have gotten to many 6s in a row.
	 * The max is 2. You can get two 6s and then one 5 for the best move. however
	 * if you throw three 6s it is emidently nextPlayer()
	 *
	 * We also check if the user can do any moves. if not, we call nextPlayer();
	 *
	 * @param DiceValue
	 * @return diceValue or 0.
	 */
	public int throwDice(int DiceValue) {
		this.lastDiceValue = DiceValue;
		System.out.println("throwDice:");
		boolean hasNoActivePieces = hasNoActivePieces();
		throwCount++;
		threwAnSix = (DiceValue == 6);
		/**
		 * 	If this is the first Throw, we must adjuct the status - For now, just always set it.
		 */
		setStatus("Started");

		new DiceEvent(this, this.activePlayer, DiceValue);

		/**
		 * If the user has all their pieces at start, they have three attempts to get an 6
 		 */
		if(hasNoActivePieces && !threwAnSix) {
			if( throwCount < 3) {
				System.out.println("INC:");
				new PlayerEvent(this,this.activePlayer, 1);
				return 0;
			}
			else{
				System.out.println("CurPlayer - " + activePlayer);
				nextPlayer();
				return 0;
			}
		}

		/**
		 * Could find this rule anywhere but is related to line 267 in the test.
		 * Means that a user cant get 3 6s after each other.
		 */
		if(!hasNoActivePieces && threwAnSix){
			if(throwCount >= 3) {
				nextPlayer();
				return DiceValue;
			}
		}

		/**
		 * Check if there is any valid moves the player can make
		 * e.g if there is a tower in between..
		 */
		if(!anyLegalMoves(DiceValue)) {
			System.out.println("!AnyLegalMoves:");

			nextPlayer();
		}
		return DiceValue;

	}

	/**
	 * Checks if the active user can do any moves.
	 * @param value - diceValue
	 * @return true if the active user can do any legal moves.
	 */
	private boolean anyLegalMoves(int value) {
		int maxBoardLength = 59;
		this.banlist = new Vector();
		System.out.println("AnyLegalMoves: " + Tower.size());

		/** If one try to reach the goal, you need an exact number to win
		 * We also ban things still at the start if they didnt get a six
		 */
		for(int p = 0; p<4; p++){
			if((getPosition(activePlayer,p) + value > maxBoardLength) || ((getPosition(activePlayer,p) == 0) && (value != 6))){
				System.out.println("banned because maxLentht "+ value +" " + getPosition(activePlayer,p) );
				this.banlist.add(p);
			}
		}
		/**
		 * 	Check if there is any that wont hit an tower.
		 */
		for(int i = 0; i<Tower.size(); i++){
			int towerPos = (int)Tower.elementAt(i);
			for(int piece = 0; piece < 4; piece++){
				if(this.banlist.contains(piece)) continue;
				int usrpos = getPosition(activePlayer,piece);
				int pos = userGridToLudoBoardGrid(activePlayer, usrpos);
				System.out.println("Pos " + pos + " towerPos " + towerPos);

				/** Will hit a tower */
				if(pos >= towerPos - value && pos < towerPos){
					if(usrpos + value != maxBoardLength) {
						/** We are not blocked if the tower is our own */
						if(notOurTower(towerPos,activePlayer())) {
							System.out.println("banned because a tower");
							this.banlist.add(piece);
						}
					}
				}
			}

		}

		if(this.banlist.size() != 4) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the tower in question is our own.
	 * Usefull as we can move past our own towers.
	 * @param towerPos
	 * @param player
	 * @return
	 */
	private boolean notOurTower(int towerPos, Integer player) {
		for(int piece = 0; piece < 4; piece++){
			int piecePos = getPosition(activePlayer,piece);
			if(towerPos == userGridToLudoBoardGrid(activePlayer, piecePos))
				return false;
		}
		return true;
	}

	/**
	 * Checks if the player have any active pieces not at start or goal.
	 * @return true if the player has no pieces not at start or in goal.
	 */
	private boolean hasNoActivePieces() {
		for(int piece = 0; piece < 4; piece++){
			if((0 != this.getPosition(activePlayer,	piece))
					&& (59 != this.getPosition(activePlayer,	piece))) return false;
		}
		return true;
	}

	/**
	 * Handles a movePiece event from the client.
	 * If we recieved two movePiece events, inbetween each other. We return false.
	 * For the correct piece - do the move
	 * @param playerID
	 * @param from
	 * @param to
	 * @return false if didnt find piece or sent two movePiece too fast.
	 */
	public boolean movePiece(int playerID, int from, int to) {
		System.out.println("MovePiece:");
		/** If tries to move before rolling a dice again: */
		if(this.lastDiceValue == 0) return false;
		/** Find out which piece that is on */
		for (int piece = 0; piece < 4; piece++)
			if(getPosition(playerID,piece) == from)
			{
				checkIfPlayerAtPosition(playerID, to);

				checkIfWeMakeATower(playerID, to);

				setPosition(playerID,piece, to);

				removeTowerIfWeStartedAtATower(playerID, piece, from);

				new PieceEvent(this, playerID,piece,from,to);

				if(weWon()) {
					setStatus("Finished");
					/** Raise an event saying we won the game. */
					new PlayerEvent(this,getWinner(),PlayerEvent.WON );
				}

				/**
				 * Moving a piece out from start, means you wont get a extra move
				 * And not throwing a 6 wont give you a extra move
				 */
				if(!threwAnSix || (threwAnSix && from==0))
					nextPlayer();

				/**
				 * If you didnt move out from start and got a 6
				 * You get a new move.
				 */
				if(threwAnSix && from != 0)
					new PlayerEvent(this,playerID,1 );

				lastDiceValue = 0;
				return true;
			}
		lastDiceValue = 0;
		return false;
	}

	/**
	 * If we leave a tower, we have to remove the flag we set to mark where the boards towers are.
	 * @param playerID
	 * @param piece
	 * @param from
	 */
	private void removeTowerIfWeStartedAtATower(int playerID, int piece, int from) {

		int pos = userGridToLudoBoardGrid(playerID, from);
		if (Tower.contains(pos)) {
			/**
			 * Remove the tower flag as we move away.
			 * if we dont still make a tower there e.g we had a tower of more than 2 pieces there.
			 */
			if(!stillTowerAt(from))
				Tower.removeElement(pos);
		}

	}

	/**
	 * Checks if we still have a tower at a position
	 *
	 * @param towerpos - the value seen from the players user grid.
	 * @return true if still a tower
	 */
	private boolean stillTowerAt(int towerpos) {
		int count = 0;
		for( int piece = 0; piece < 4; piece++)
			if(getPosition(activePlayer,piece) == towerpos)
				count++;

		return count >=2;
	}

	/**
	 * Checks if we won. If all of our pieces is at the fininsh (nr 59) it means
	 * we won. Update the winner.
	 * @return
	 */
	private boolean weWon() {
		int winPosition = 59;
		for(int piece = 0; piece<4; piece++)
			if(getPosition(activePlayer,piece) != winPosition) return false;

		/** We won */
		winner = activePlayer;
		return true;
	}

	/**
	 * Checks if we already have a piece there, is so set a tower Flag.
	 * Another approach is to dynamically check each time we roll a dice. However I think this is better.
	 * @param playerID
	 * @param to
	 */
	private void checkIfWeMakeATower(int playerID, int to) {
		for(int i = 0; i< 4; i++){
			if( to == playerPieces[playerID][i])
			{
				setTowerFlagAt(playerID, to);
			}
		}
	}



	private void setTowerFlagAt(int playerID, int to) {
		Tower.addElement(userGridToLudoBoardGrid(playerID,to));
	}

	/**
	 * Checks if we hit a player when we move. This also takes care of moving
	 * the other player back to start if that happens.
	 * @param playerID
	 * @param to
	 */
	private void checkIfPlayerAtPosition(int playerID, int to) {
		int pos = userGridToLudoBoardGrid(playerID, to);

		for(int player = 0; player < players.size(); player++){
			if ( player == playerID) continue;
			for(int i = 0; i< 4; i++){
				if( pos == userGridToLudoBoardGrid(player, playerPieces[player][i])){

					//move player back to start
					// Tell client to draw GUI of the move
					setPosition(player,i,0);
					drawGuiForMove(player, i, playerPieces[player][i], 0);

				}
			}
		}
	}

	/**
	 * We cant just raise a moveEvent, as move would call canMove() and that will fail.
	 * This takes care of that.
	 * @param player
	 * @param piece
	 * @param from
	 * @param to
	 */
	private void drawGuiForMove(int player, int piece, int from, int to) {
		HashMap<String, Object> msg = new HashMap<>();
		msg.put("TYPE","DRAWGUIFORMOVE");
		msg.put("PLAYER",player);
		msg.put("PIECE",piece );
		msg.put("FROM", from);
		msg.put("TO",to);
		msg.put("ID",this.getId());
		ClientConnection.getConnection().throwEventForGameListner(msg);
	}

	private void setPosition(int playerID, int piece, int to) {
		playerPieces[playerID][piece] = to;
	}

	/**
	 * GetStatus
	 * @return the status of the game
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * get the color of the games winner.
	 * @return -1 if no winner, or the winners collor
	 */
	public Integer getWinner() {
		return winner;
	}

	/**
	 * Converts between userGrid ( What the players see ) and the board grid.
	 * @param playerColor
	 * @param userPos
	 * @return the boardGrid value for that position.
	 */
	public int userGridToLudoBoardGrid(int playerColor, int userPos) {
		return userGridToPlayerGrid[userPos][playerColor];
	}

	/**
	 * adds a new DiceListner.
	 * @param diceListener1
	 */
	public void addDiceListener(DiceListener diceListener1) {
		diceListeners.add(diceListener1);
	}


	/**
	 * adds a new pieceListner.
	 * @param pieceListener1
	 */
	public void addPieceListener(PieceListener pieceListener1) {

		pieceListener.add(pieceListener1);

	}


	/**
	 * Adds a player listner.
	 * @param playerListener1
	 */
	public void addPlayerListener(PlayerListener playerListener1) {
		playerListener.add(playerListener1);
	}

	/**
	 * Changes the active player to the next player.
	 *
	 * Also has to reset ThrowCount and lastDiceValue.
	 */
	public void nextPlayer() {
		lastDiceValue = 0;
		throwCount=0;

		activePlayer = (++activePlayer)%players.size();
		if(players.get(activePlayer).contains(inactive)) {

			nextPlayer();
			return;
		}
		System.out.println("New player is " + activePlayer);

		new PlayerEvent(this, activePlayer,PlayerEvent.PLAYING);
	}

	/**
	 * Can a player move piece.
	 * @param piece
	 * @return true if the piece is movable.
	 */
	public boolean canMove(int piece){
		return !banlist.contains((int)piece);
	}

	/**
	 * Checks if a player can move any of the pieces.
	 * TODO - cant we here just check if banlist.size() != 4?
	 * @return true if there is any pieces the player can move.
	 */
	public boolean canMove() {
		if(anyLegalMoves(lastDiceValue))
			return true;
		return false;
	}

	/**
	 * Set the id of the game
	 * @param gameId
	 */
	public void setId(String gameId) {
		this.id = gameId;
	}
	/**
	 * @return the id of the game
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * get the value of the last Dice throw.
	 * @return diceValue, or 0 if it have been handled.
	 */
	public int getlastDiceValue() {
		return lastDiceValue;
	}

	/**
	 * Reset a players pieces. Used in case a player become unactive.
	 * Also remember to reset any eventual Towers these pieces had made
	 * @param activePlayer
	 */
	public void removePlayerPieces(int activePlayer) {
		for(int piece = 0; piece < 4; piece++){

			int piecePosition = userGridToLudoBoardGrid(activePlayer,getPosition(activePlayer,piece));
			if(Tower.contains(piecePosition))
				Tower.removeElement(piecePosition);

			setPosition(activePlayer,piece,0);
		}
	}
	/**
	 * if only one player is active, end the game
	 */
	public void weAlone(){
		if(activePlayers()==1){
			winner = getFirstActivePlayer();
			new PlayerEvent(this,winner,PlayerEvent.WON);
			return;
		}
	}

	/**
	 * Returns the first active player.
	 * We use this when everyone except one player is inactive. To just have that single player win the game
	 * @return the player id of the first active player. Or -1 if none.
	 */
	private int getFirstActivePlayer() {
		int player = 0;
		for(; player<players.size(); player++)
			if(!players.get(player).contains(inactive))
				return player;
		return - 1;
	}
}