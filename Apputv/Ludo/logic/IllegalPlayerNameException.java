package no.ntnu.imt3281.ludo.logic;

/**
 * IlligalPlayerNameException - If someone tries to create a player with an iligal name.
 */
public class IllegalPlayerNameException extends Exception {

	/**
	 * IllegalPlayerNameException
	 */
	public IllegalPlayerNameException() {
		super();
	}

	/**
	 * IllegalPlayerNameException
	 * @param message
	 */
	public IllegalPlayerNameException(String message) {
		super(message);
	}
}
