package no.ntnu.imt3281.ludo.logic;

/**
 * Dice listner. Used when listning to DiceEvent throws from the ludo game.
 */
public interface DiceListener {

	/**
	 * DiceThrown.
	 * @param diceEvent1
	 */
	 void diceThrown(DiceEvent diceEvent1);
}
