package no.ntnu.imt3281.ludo.logic;

/**
 * PlayerEvent.
 * Used when its a new players turn, a player left the game, or someone won.
 */
public class PlayerEvent {

	/**
	 * Game has not yet started.
	 */
	public static final int WAITING = 0;
	/**
	 * Its a players turn to do something.
	 */
	public static final int PLAYING = 1;
	/**
	 * A player left the game ( Became inactive )
	 */
	public static final int LEFTGAME = 2;
	/**
	 * The game is finished, and player won.
	 */
	public static final int WON = 3;


	Integer activePlayer;
	Integer state;

	/**
	 * Tell all the listners about the event.
	 * @param ludo
	 * @param Player
	 * @param State
	 */
	public PlayerEvent(Ludo ludo, Integer Player, Integer State) {
		setActivePlayer(Player);
		setState(State);
		for (PlayerListener hl : ludo.playerListener)
			hl.playerStateChanged(this);
	}

	/**
	 * Get the player in question.
	 * @return the players color.
	 */
	public Integer getActivePlayer() {
		return activePlayer;
		
	}

	/**
	 * Set the player in question.
	 * @param player
	 */
	public void setActivePlayer(Integer player) {
		activePlayer = player;
	}

	/**
	 * Get the event state.
	 * @return
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Set the event state for this event.
	 * @param playerState
	 */
	public void setState(Integer playerState) {
		state = playerState;
	}

}
