package no.ntnu.imt3281.ludo.logic;

/**
 * PlayerListner - used for listning to:
 * LeftGame, Won, Or just events telling whos turn it is.
 */
public interface PlayerListener {

	/**
	 * Handle the playerStateChange.
	 * @param ple
	 */
	 void playerStateChanged(PlayerEvent ple);
}
