package no.ntnu.imt3281.ludo.client;

import java.net.Socket;
import java.net.UnknownHostException;

import javafx.application.Platform;
import no.ntnu.imt3281.ludo.gui.*;
import no.ntnu.imt3281.ludo.logic.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * ClientConnection. Holds the user socket and handles messages from user to server.
 * Also delegates messages it recievs from the server to the correct handler.
 */
public class ClientConnection {
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);
	static ClientConnection instance = null;
	
	
	static LudoController ludoController = null;
	static LoginController loginController = null;

	UserData userinfo = new UserData();
	private ArrayList<GameBoardController> games = new ArrayList<>();
	private HashMap<String,ArrayList<GameListener>> Listners = new HashMap<>();

	private ObjectInputStream is;
	private ObjectOutputStream os = null;
	private Socket socket;

	/**
	 * The server port
	 */
	public static final int SERVER_PORT = 8000;
	/**
	 * The server address.
	 */
	public static final String SERVER_ADDRESS = "localhost";

	/**
	 * Get user info. Asks the server for userinfo for a given user.
	 * @param name
	 * @return null
	 */
	public Map<String,Object> getUserInfo(String name) {
		HashMap<String,Object> map = new HashMap<>();
		map.put("TYPE", "GETUSERINFO");
		if(name != "")
			map.put("USERNAME", name);
		else
			map.put("USERNAME", this.userinfo.getUserName());
		try{
			os.writeObject(map);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "HashMap", e);
		}
		return null;
	}

	/**
	 * insert the listner to a global map, so we can call the correct listner based on the key.
	 * @param key
	 * @param listner
	 */
	public void addGameListner(String key, GameListener listner) {
		ArrayList<GameListener> list = Listners.get(key);
		if(list == null) list = new ArrayList<>();

		list.add(listner);
		Listners.put(key,list);
	}

	/**
	 * Ask the server for an updated playerList for Group with id = groupId.
	 * @param groupId
	 */
	public static void playerListRequest(int groupId) {
		getConnection().sendServerGameMessage("PLAYERLISTCHANNEL", groupId);

	}

	private void sendServerGameMessage(String type, int groupId) {
		HashMap<String, Object>msg = new HashMap<>();
		msg.put("TYPE", type);
		msg.put("GROUPID", groupId);
		sendMSG(msg);
	}

	/**
	 * Get username.
	 * @return the username of this client.
	 */
	public String getUserName() {
		return userinfo.getUserName();
	}

	/**
	 * View userprofile for.
	 * Opens the userprofile for the given user.
	 * @param user
	 */
	public void viewProfileFor(String user) {
		ludoController.viewUserProfile(user);
	}



	/**
	 * ClientConnection constructor.
	 * Sets up the socket, object input/outstream and starts the thread doing ping and the thread waiting for response.
	 */
	public ClientConnection() {
		try {
			socket = new Socket(SERVER_ADDRESS, SERVER_PORT);

			this.is = new ObjectInputStream(socket.getInputStream());

			this.os = new ObjectOutputStream(socket.getOutputStream());
			instance = this;
			sendPingRequest();
			waitForResponse();
		} catch (UnknownHostException e) {
			logger.throwing(this.getClass().getName(), "ClientConnection", e);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "ClientConncetion", e);
		}
	}

	/**
	 * Check for new messages - using hashmap, not msg
	 */
	private void CheckForNewMsg(){
		try {
				HashMap<String, Object> msg = (HashMap<String, Object>) is.readObject();

			switch((String)msg.get("TYPE")){
					case "LOGINREPLY":
						LoginHandler(msg);
						break;
					case "GAMEINVITE":
						ludoController.invitePlayer(msg);
						break;
					case "NEWJOINEDGAME":
						ludoController.createNewGame(msg);
						break;
					case "REGISTERREPLY":
						RegisterHandler(msg);
						break;
					case "PIECEMOVED":
						break;
					case "AUTOLOGIN":
						autoLoginHandler(msg);
						break;
					case "NEWCHATNOTIFY":
						handleNewChatNotify(msg);
						break;
				default:
					break;
					
				}

			throwEventForGameListner(msg);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "CheckForNewMsg", e);
		} catch (ClassNotFoundException e) {
			logger.throwing(this.getClass().getName(), "CheckForNewMsg", e);
		}


	}

	/**
	 * Opens the GUI for a chat, given a HashMap containing a chatID
	 * @param msg hashmap containing "GROUPID"
	 */
	private void handleNewChatNotify(HashMap<String, Object> msg) {
		int groupid = (int)msg.get("GROUPID");
		this.handleNewChatNotify(groupid);
	}

	/**
	 * Opens the GUI for a chat, given a chatID
	 * @param groupid
	 */
	public void handleNewChatNotify(int groupid){
		Platform.runLater(()->this.ludoController.setupNewChat(groupid));
	}

	/**
	 * If the msg contains a type someone is subscribing to - throw an event
	 * @param msg
	 */
	public void throwEventForGameListner(Map<String,Object> msg) {
		String type = (String)msg.get("TYPE");
		ArrayList<GameListener> ListnerList = Listners.get(type);
		if(ListnerList != null) {
			for (GameListener listner :ListnerList)
				listner.GameChange(msg);
		}
	}


	private void RegisterHandler(HashMap<String, Object> msg) {
		userinfo.setToken((String)msg.get("TOKEN"));
		userinfo.setUserName((String)msg.get("USERNAME"));
		loginController.registerResponse(userinfo.getToken());
			if (!"TOKENFAILURE".equals(userinfo.getToken())){
				userinfo.setLoggedIn((Boolean) msg.get("isLoggedIn"));
				this.userinfo.setID((int)msg.get("ID"));
				ludoController.setLoginMessage(userinfo.getUserName(), userinfo.getToken());
					if(loginController.getCheckBox()) {
						writeToFile(true, userinfo.getToken());
					} else {
						writeToFile(false, userinfo.getToken());
					}
			}
	}
	
	private void LoginHandler(HashMap<String, Object> msg){
		userinfo.setToken((String)msg.get("TOKEN"));
			if ("TOKENFAILURE".equals(userinfo.getToken())) {
				loginController.loginResponse(userinfo.getToken());
			} else {
				this.userinfo.setUserName((String)msg.get("USERNAME"));
				this.userinfo.setLoggedIn((Boolean) msg.get("isLoggedIn"));
				this.userinfo.setID((int) msg.get("ID"));
				loginController.loginResponse(userinfo.getToken());
				ludoController.setLoginMessage(userinfo.getUserName(), userinfo.getToken());
			}

	}

	private void autoLoginHandler(HashMap<String, Object> msg){
		userinfo.setToken((String)msg.get("TOKEN"));
			if ("TOKENFAILURE".equals(userinfo.getToken())) {
				loginController.loginResponse(userinfo.getToken());
			} else {
				userinfo.setUserName((String)msg.get("USERNAME"));
				userinfo.setLoggedIn((Boolean) msg.get("isLoggedIn"));
				userinfo.setID( (int) msg.get("ID"));
				ludoController.setLoginMessage(userinfo.getUserName(), userinfo.getToken());
			}

	}

	/**
	 * Method to set the controller, this will be used to update client, closing tabs etc.
	 * @param controller
	 */
	public static void setLudoController(LudoController controller) {
		ludoController = controller;
	}
	/**
	 * Method to set the controller, this will be used to update client, closing tabs etc.
	 * @param controller
	 */
	public static void setLoginController(LoginController controller) {
		loginController = controller;
	}
	/**
	 * A Thread whose soule porpose is to check if there are any new messages,  and handle them
	 *
	 */
	private void waitForResponse(){
		Runnable newTask = () -> {
			while(true){
					CheckForNewMsg();

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					logger.throwing(this.getClass().getName(), "waitForResponse", e);
					Thread.currentThread().interrupt();
				}
			}
		};

		new Thread(newTask).start();
	}

	private void sendPingRequest() {
		Runnable newTask = () -> {
			while(true) {
				if(this.userinfo.getLoggedIn()) {
	 				HashMap<String, Object> msg = new HashMap<>();
					msg.put("TYPE", "PING");
					sendMSG(msg);
				}
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					logger.throwing(this.getClass().getName(), "sendPingRequest", e);
					Thread.currentThread().interrupt();

				}
			}
		};
		new Thread(newTask).start();
	}

	/**
	 * The instance of the connection
	 * @return the instance of the connection
	 */
	public static ClientConnection getConnection() {
		if(instance == null){
			instance = new ClientConnection();
		}
		return instance;
	}
	/**
	 * Sending messages to the server
	 * @param message to send
	 * @param type of message to send GAME/CHAT
	*/
	public static void sendGameMessage(String message, String type, String id) {
		getConnection().sendServerGameMessage(message, type, id);
	}

	/**
	 * Sending messages to the server
	 * @param message to send
	 * @param type of message to send JOINCHAT
	 */
	public static void sendJoinMessage(String message, String type){
		getConnection().sendServerJoinMessage(message, type);
	}

	/**
	 * Sending messages to the server
	 * @param message to send
	 * @param type of message to send NEWCHAT
	 */
	public static void sendNewChatMessage(String message, String type){
		getConnection().sendServerNewChatMessage(message, type);
	}

	/**
	 * Sends a message to the server with the given type, username and password arguments
	 * @param type
	 * @param UserName
	 * @param password
	 */
	public void sendSrvMsg(String type, String UserName, String password){
		HashMap<String,Object> message = new HashMap<>();
		message.put("TOKEN", this.userinfo.getToken());
		message.put("TYPE", type);
		message.put("USERNAME", UserName);
		message.put("PASSWORD", password);


		try {
			this.os.writeObject(message);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendSrvMsg", e);
		}
	}

	/**
	 * Send text. Used when sending a chat message to a group.
	 * @param message
	 * @param type
	 * @param groupID
	 */
	public static void sendText(String message, String type, String groupID){
		getConnection().sendServerChat(message,type,groupID);
	}

	/**
	 * Send server chat.  Used when sending a chat message to a group.
	 * @param message
	 * @param type
	 * @param groupID
	 */
	private void sendServerChat(String message, String type, String groupID) {
		HashMap<String,Object> msg = new HashMap<>();
		msg.put("TOKEN", userinfo.getToken());
		msg.put("TYPE", type);
		msg.put("GROUPID", groupID);
		msg.put("MESSAGE", message);
		try {
			os.writeObject(msg);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendServerChat", e);
		}
	}


	/**
	 * Ask the server for a list of channels. Either that you are a member of, or you are not.
	 * @param memberIn - if you are a member of the channels or not.
	 */
	public static void channelListRequest(boolean memberIn){
		String type = "CHANNELLIST";
		if(memberIn)	type = "CHANNELLISTMEMBER";
		getConnection().sendServerGameMessage(type);

	}

	/**
	 * Request a playerList from the server.
	 */
	public static void playerListRequest() {
		getConnection().sendServerGameMessage("PLAYERLIST");
	}


	/**
	 * Send a message to the server with the given arguments.
	 * @param type
	 * @param username
	 * @param id
	 */
	public void sendServerGameMessage(String type, String username, String id) {
		HashMap<String,Object> message = new HashMap<>();
		message.put("TYPE", type);
		message.put("TOKEN", this.userinfo.getToken());
		message.put("USERNAME", username);
		message.put("ID", id);

		try {
			this.os.writeObject(message);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendServerGameMessage", e);
		}
	}

	/**
	 * Send a message to the server with the given arguments.
	 * @param type
	 */
	public void sendServerGameMessage(String type) {
		sendServerGameMessage(type, this.userinfo.getUserName(),String.valueOf(this.userinfo.getID()));

	}

	/**
	 * Sends the joinmessage to server, with the type to listen on.
	 * Sets the users token so we can use it serverside.
	 *
	 * @param  grpID  the group id of the group you want to join
	 * @param  type the type you want to listen on. This case JOINCHAT
	 */
	public void sendServerJoinMessage(String grpID, String type) {
		HashMap<String,Object> message = new HashMap<>();
		message.put("GROUPID", grpID);
		message.put("TYPE", type);
		message.put("TOKEN", this.userinfo.getToken());

		try {
			this.os.writeObject(message);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendServerJoinMessage", e);
		}
	}
	/**
	 * Sends the newchatmessage to server, with the type to listen on.
	 * Sets the users token so we can use it serverside.
	 *
	 * @param  userName  the username of the user you want to create a chat with
	 * @param  type the type you want to listen on. This case NEWCHAT
	 */
	public void sendServerNewChatMessage(String userName, String type) {
		HashMap<String,Object> message = new HashMap<>();
		message.put("USERNAME", userName);
		message.put("TYPE", type);
		message.put("TOKEN", this.userinfo.getToken());

		try {
			this.os.writeObject(message);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendServerNewChatMessage", e);
		}
	}

	/**
	 * Send the HashMap to the server.
	 * @param map
	 */
	public void sendMSG(Map<String, Object> map){
		HashMap<String,Object>msg = (HashMap<String, Object>) map;
		try {
			msg.put("TOKEN", this.userinfo.getToken());
			msg.put("ID", this.userinfo.getID());
			this.os.writeObject(msg);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendMSG", e);
		}
	}
	/**
	 * Sends disconnect message to server and stops the client from reading more messages
	 */
	public static void stopConnection() {
		HashMap<String,Object> stopUser = new HashMap<>();
		stopUser.put("TYPE", "DISCONNECTED");
		getConnection().sendMSG(stopUser);
		
		try {
			getConnection().is.close();
		} catch (IOException e) {
			logger.log(Level.FINER, "stopConnection", e);
		}
	}
	
	/**
	 * Adding a game to the list of games
	 * @param game to add
	 */
	public static void addGame(GameBoardController game) {
		//Use for getting the game when a player wants to join the newly created game
		getConnection().games.add(game);
		
	}

	/**
	 * Check if the user is logged in.
	 * @return true if he is.
	 */
	public static boolean isLoggedIn() {
		return getConnection().userinfo.getLoggedIn();
	}
	/**
	 * Method to write userdata (token) to file with a boolean check to see if the user want to login automatically
	 * or not.
	 * @param check true or false depending on if the user want to log in automatically or not.
	 * @param token to validate the user to the server without using username/password.
	 */
	public void writeToFile(boolean check, String token) {
		try {
			PrintWriter writer = new PrintWriter("autologin.txt", "UTF-8");
			writer.println(check);
			writer.println(token);
			writer.close();
		} catch (Exception e) {
			logger.throwing(this.getClass().getName(), "writeToFile", e);
		}
		
	}
	/**
	 * Method to read token from file for auto login. If the method contains boolean true, it will send the token to server for verification.
	 */
	public void readFromFile() {
		
		try {
			File file = new File("autologin.txt");
			if (file.exists()) {
				FileReader fileReader = new FileReader(file);
				BufferedReader first = new BufferedReader(fileReader);
				String bool = first.readLine();
				if(bool != null) {
					String tok = first.readLine();
					if ("true".equals(bool)) {
						tokenMsg(tok);
					}
				}
			first.close();
			fileReader.close();
			}
		} catch (Exception e) {
			logger.throwing(this.getClass().getName(), "readFromFile", e);
		}
	} 
	/**
	 * Method to send token for autologin to server, to verify the user.
	 * @param token taken from local file.
	 */
	public void tokenMsg(String token) {
		HashMap<String,Object> message = new HashMap<>();
		message.put("TYPE", "AUTOLOGIN");
		message.put("TOKEN", token);
		try {
			this.os.writeObject(message);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendServerGameMessage", e);
		}
	}
	
	/**
	 * Request a top 10 list for players who have won and played games from the server.
	 */
	public static void rankingListRequest() {
		getConnection().sendServerGameMessage("RANKINGLIST");
	}
}
