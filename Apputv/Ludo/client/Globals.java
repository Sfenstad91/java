package no.ntnu.imt3281.ludo.client;
/**
 * Class to hold global variables like log name, port and server adress.
 * @author Martin
 *
 */
public class Globals {
	
	public static final String LOG_NAME = "LudoLog";
	
	/**
	 * Private constructor to prevent instances of this class.
	 */
	private Globals() {}
}
