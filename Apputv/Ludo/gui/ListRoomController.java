package no.ntnu.imt3281.ludo.gui;


import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.logic.GameListener;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * This is the main class for opening chat rooms.
 * It allows the user to see a list of the chat rooms that you are a member of.
 * And the ability to open the chat rooms.
 *
 * @author Cim
 *
 */
public class ListRoomController {
    @FXML
    private ListView<String> list;

    /**
     * requesting list of rooms from server
     */
    @FXML
    protected void initialize() {

        GameListener channellist = data -> Platform.runLater(()-> handleNewChannelList(data));
        ClientConnection.getConnection().addGameListner("CHANNELLISTMEMBER",channellist);

        ClientConnection.channelListRequest(true);
    }

    /**
     * Handles ChannelList updates from the server
     * @param data
     */
    private void handleNewChannelList(Map<String, Object> data) {
        ArrayList<Integer> channels = (ArrayList<Integer>) data.get("CHANNELLIST");
        if(channels == null) return;

        ObservableList<String> RoomList = FXCollections.observableArrayList();
        for(Integer channelID: channels){
            RoomList.add(channelID.toString());
        }
        list.setItems(RoomList);
    }

    /**
     * Opens the tab for the selected chatroom
     */
    public void openRoom() {
        int groupID =Integer.parseInt(list.getSelectionModel().getSelectedItem());
        ClientConnection.getConnection().handleNewChatNotify(groupID);
    }
}
