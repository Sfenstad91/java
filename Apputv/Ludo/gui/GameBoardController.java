package no.ntnu.imt3281.ludo.gui;

/**
 * Sample Skeleton for 'GameBoard.fxml' Controller Class
 */

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import java.awt.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.logic.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * GameBoard controller - contains all the information about an ongoing game.
 */
public class GameBoardController {
    String playerPictures[] = {"redPiece.png","bluePiece.png","yellowPiece.png","greenPiece.png"};
    @FXML
    private Label player1Name;

    @FXML
    private ImageView player1Active;

    @FXML
    private Label player2Name;

    @FXML
    private ImageView player2Active;

    @FXML
    private Label player3Name;

    @FXML
    private ImageView player3Active;

    @FXML
    private Label player4Name;

    @FXML
    private ImageView player4Active;

    @FXML
    private ImageView diceThrown;

    @FXML
    private Button throwTheDice;

    @FXML
    private TextArea chatArea;

    @FXML
    private TextField textToSay;

    @FXML
    private Button sendTextButton;
    
    @FXML
    private Label winnerLabel;

    @FXML
    private AnchorPane anchor;
    int newpos= 16;
    boolean first = true;
    
    private Ludo ludo;
    private int playerNumber;
	private AnchorPane gameBoard;
    private boolean piecesMovable;
    private int chatID;

    /**
     * GameBoardController, set up the ludoGame and initialise the listners.
     */
    public GameBoardController() {
		ludo = new Ludo();
		ClientConnection.addGame(this);
        initListners();
        TopLeftCorner(); // Generate the array of boardPositions for the pieces(X,Y)
        this.piecesMovable = false;
	}

    /**
     * Setup all the listners we want to use for the gameboard controller.
     */
    private void initListners() {

        /**
         * Set up listneres for event thrown in clientConnection, when we recieve a Game message
         * From the server.
         */
        GameListener  diceThrown  = data -> Platform.runLater(()->{
                    diceThrown(data);
                });
        ClientConnection.getConnection().addGameListner("DICETHROWN", diceThrown);

        GameListener  pieceMoved  = data -> Platform.runLater(()->{
            pieceMoved(data);
        });
        ClientConnection.getConnection().addGameListner("PIECEMOVED", pieceMoved);

        GameListener playerJoined = data -> Platform.runLater(()-> {
            newPlayerHandler(data);
        });
        ClientConnection.getConnection().addGameListner("PLAYERJOINED",playerJoined);


        /**
         *  handles Relocate a player events
         */
        GameListener relocatePlayer = data -> Platform.runLater(()-> {
            updatePlayerPosition(data);
        });
        ClientConnection.getConnection().addGameListner("DRAWGUIFORMOVE",relocatePlayer);
        /**
         * When four joins a game automatically start the game
         */
        GameListener startGame = data -> Platform.runLater(()-> {
            throwTheDice.setText(I18N.getBundle().getString("ludogameboard.throwDiceButton"));
            showActivePlayer(0);
        });
        ClientConnection.getConnection().addGameListner("STARTGAME", startGame);
        /**
         * Player left the game, soo remove his pieces.
         */
        GameListener playerLeft = data -> Platform.runLater(()-> {
            new PlayerEvent(this.ludo,(int)data.get("PLAYER"), (int)data.get("STATE"));
        });
        ClientConnection.getConnection().addGameListner("STATECHANGE",playerLeft);

        /**
         * Recieved a chatmessage.
         */
        GameListener chatmsg = data -> Platform.runLater(()-> {
            updateChat(data);
        });
        ClientConnection.getConnection().addGameListner("CHATMESSAGEREPLY",chatmsg);


        /**
         * Set up listners for event thrown locally by Ludo
         */
        DiceListener diceListener = diceThrow -> Platform.runLater(() -> {
            diceThrowHandler(diceThrow);
        });

        PieceListener pieceListener = pieceMove -> Platform.runLater(() -> {
            pieceMovedHandler(pieceMove);
        });

        PlayerListener playerListener = playerevent -> Platform.runLater(() -> {
            playerEventHandler(playerevent);
        });

        ludo.addPieceListener(pieceListener);
        ludo.addDiceListener(diceListener);
        ludo.addPlayerListener(playerListener);


    }

    /**
     * Update the chat with the new chatmessage we got.
     * @param data
     */
    private void updateChat(Map<String, Object> data) {
        if ((int)data.get("GROUPID") == this.chatID){ // correct id
            chatArea.setWrapText(true);
            chatArea.appendText(data.get("USERNAMEOFSENDER") + ": " + data.get("MESSAGE") + "\n");
        }
    }

    /**
     * Update the GUI of a user move
     * @param data
     */
    private void updatePlayerPosition(Map<String, Object> data) {
        if(data.get("ID") != ludo.getId()) return;
        // Get the players new position
        int newpos = ludo.userGridToLudoBoardGrid((int)data.get("PLAYER"),(int)data.get("TO"));
        relocatePiece((int)data.get("PLAYER"),(int)data.get("PIECE"),newpos);

    }

    /**
     * When the server sends info about a new player, we also need to add them.
     * @param data
     */
    private void newPlayerHandler(Map<String, Object> data) {
        if (data.get("ID") == ludo.getId()) {
            ludo.addPlayer((String)data.get("USERNAME"));
            setChatID((int)data.get("CHATID"));
            // Before drawing the new pieces, we need to remove all the old one
            anchor.getChildren().remove(1,(ludo.activePlayers()-1)*4 + 1);

            drawAllPieces(ludo.activePlayers());
            updatePlayerNames();
            if(ludo.nrOfPlayers() > 1) {
            	throwTheDice.setText(I18N.getBundle().getString("ludogameboard.redStarts"));
            	throwTheDice.setDisable(false);
            }
        }
    }
    /**
     * Handles PieceMoved event from the client.
     * Used to update the GUI of the PIECE
     * @param pieceMove
     */
    private void pieceMovedHandler(PieceEvent pieceMove) {
        int newpos = ludo.userGridToLudoBoardGrid(pieceMove.getPlayer(),pieceMove.getTo());
        System.out.println("NewPos "+ newpos + " to " + pieceMove.getTo());
	    relocatePiece(pieceMove.getPlayer(),pieceMove.getPiece(),newpos);
    }

    /**
     * Handles pieceMove events from the server
     * Used to call movePiece in the local ludo game.
     * @param data
     */
    private void pieceMoved(Map<String, Object> data) {
        if(data.get("ID") == ludo.getId()){
            /**
             *  Do the movePiece, and handle the pieceMovedEvent, locally.
             */
            ludo.movePiece((int)data.get("COLOR"),(int)data.get("FROM"),(int) data.get("TO"));
        }
    }

    /**
     * Handle function for local playerEvents
     * @param playerevent
     */
    private void playerEventHandler(PlayerEvent playerevent) {
	    int player = playerevent.getActivePlayer();

        switch(playerevent.getState()) {
            case (PlayerEvent.WAITING):
                /**
                 *  The game have not started yet
                 */
                break;
            case(PlayerEvent.PLAYING):
                /**
                 * If we are the active player.
                 */
                if (playerNumber == player){
                	showActivePlayer(player);
                	throwTheDice.setText(I18N.getBundle().getString("ludogameboard.throwDiceButton"));
                    /**
                     *  We can now throw a dice.
                     */
                    throwTheDice.setDisable(false);
                    /**
                     * We can not move a piece yet, so for now
                     */
                    this.piecesMovable = false;
                }
                else{
                    /**
                     * if its another players turn, make sure we cant move anything
                     */
                    showActivePlayer(player);
                	throwTheDice.setText(I18N.getBundle().getString("ludogameboard.opponentsTurn"));
                    throwTheDice.setDisable(true);
                    this.piecesMovable = false;
                }
                break;
            case(PlayerEvent.LEFTGAME):
                /**
                 * Player left the game, remove his pieces.
                 */
                ludo.removePlayer(ludo.getPlayerName(playerevent.getActivePlayer()));
                ludo.removePlayerPieces(playerevent.getActivePlayer());
                resetPieces(playerevent.getActivePlayer());
                ludo.nextPlayer();
                ludo.weAlone();
                break;
            case(PlayerEvent.WON):
                /**
                 * When the game is done, users should not be able to move
                 */
                throwTheDice.setDisable(true);
                this.piecesMovable = false;

                /**
                 * We also should tell the users about who won the game
                 */
                ShowWinner(ludo.getWinner());
                break;
        }


    }

    /**
     * Resets the position for all pieces for player.
     * player*4 + piece, give us the correct starting position, given a piece
     * @param player
     */
    private void resetPieces(Integer player) {
        for(int piece = 0; piece < 4; piece++){
            relocatePiece(player,piece,player*4 + piece);
        }
    }

    /**
     * Display who won the game.
     * @param winner
     */
    private void ShowWinner(int winner) {
        
    	switch(winner) {
    	case 0:	
    		winnerLabel.setStyle("-fx-text-fill: RED;");	
    		break;
		case 1:
			winnerLabel.setStyle("-fx-text-fill: BLUE;");
			break;
		case 2:	
			winnerLabel.setStyle("-fx-text-fill: YELLOW;");
			break;
		case 3:	
			winnerLabel.setStyle("-fx-text-fill: GREEN;");	
			break;
    	}
    	winnerLabel.setText(" " + ludo.getPlayerName(winner) + I18N.getBundle().getString("ludogameboard.won"));
    	winnerLabel.setVisible(true);
    }

    /**
     * Handle function for local diceThrow events
     * @param diceThrow
     */
    private void diceThrowHandler(DiceEvent diceThrow) {
	    setDiceTo(diceThrow.getDice());
        /**
         * On the client we can change the color or something on pieces that we can move, using the blacklist thing
         * If its our turn. For now just set all the pieces to movable.
         */
        if(myTurn()){
            this.piecesMovable = true;
        }
    }

    /**
     * Tells if it is the localPlayers turn or not.
     * @return true if its the local players turn.
     */
    private boolean myTurn() {
	    return playerNumber == ludo.activePlayer();
    }

    /**
     * When the server recieves a throwDice event, call throwDice(value).
     * @param data
     */
    private void diceThrown(Map<String, Object> data) {
	    if(data.get("ID") == ludo.getId())
 	        ludo.throwDice((int)data.get("DICEVALUE"));
    }

    /**
     * User clicked on throwdice.
     * Tell the server we want to throw a dice.
     * @param e
     */
    public void throwDice(ActionEvent e){

        setThrowingDice();
        throwTheDice.setDisable(true);
        HashMap<String,Object> msg = new HashMap<>();
        msg.put("TYPE", "THROWDICE");
        msg.put("GAMEID", ludo.getId());
        ClientConnection.getConnection().sendMSG(msg);
    }

    private void setThrowingDice() {
        File file = new File("src/main/java/images/rolldice.png");
        Image img = new Image(file.toURI().toString());
        diceThrown.setImage(img);
    }

    /**
     * Sets the diceThrow image to the correct value.
     * @param value
     */
    private void setDiceTo(int value){
        String diceImage = "dice" + value + ".png";
        File file = new File("src/main/java/images/" + diceImage);
        Image img = new Image(file.toURI().toString());
        diceThrown.setImage(img);

    }

    /**
     * Called initially and should draw all the placer pieces.
     * @param players
     */
    private void drawAllPieces(int players){
        if(players >4) return;
        for(int i = 0; i< players; i++){
            for(int p = 0; p < 4; p++){
                setPlayerPieceToPos(i, i*4 + p);
            }
        }
    }


    /**
     * Will set the players position on the screen.
     * TODO - Find images for all the colors. This picture really look bad on a red backgroud.
     * @param playerID
     * @param pos
     */
    private void setPlayerPieceToPos(int playerID, int pos){
        String pic = getPlayerPicture(playerID);
        int offset = 10;
        File file = new File("src/main/java/images/" + pic);
        Image img = new Image(file.toURI().toString());
        ImageView player = new ImageView(img);
        player.setFitHeight(30);
        player.setFitWidth(30);

        /**
         * Add method to handle selection of pieces
         */
        player.setOnMouseClicked(e -> this.playerSelected(playerID, pos));

        /**
         * Find the correct X,Y
         */
        Point newPosition = BoardPlacementToCord(pos);
        if (newPosition == null) return;

        /**
         * Set the correct position
         */
        player.relocate(newPosition.getX() + offset, newPosition.getY() + offset);
        anchor.getChildren().addAll(player);

    }

    /**
     * Returns the playerImage given a player color.
     * @param playerID
     * @return
     */
    private String getPlayerPicture(int playerID) {
        if(playerID < playerPictures.length)
            return playerPictures[playerID];
        return null;
    }

    /**
     * allow the user to select a piece. We should use this when we want to tell the system what piece we want to move.
     * Currently just logs it.
     * @param playerID
     * @param pos
     */
    private void playerSelected(int playerID, int pos) {
        /**
         * If one select someone elses piece, nothing should happen.
         */
        if(playerID != this.playerNumber) return;
        System.out.println("Selected " + playerID + " piece with startpos " + pos);
        if(!piecesMovable) {
            System.out.println("Tried to move pieces, but for now we cant.");
            return;
        }

        /**
         * Tell the server we want to move this piece
         */
        HashMap<String, Object> msg = new HashMap<>();
        int pieceID = pos - (playerID*4);
        msg.put("GAMEID",this.getId());
        msg.put("TYPE","MOVEPIECE");
        msg.put("STARTPOS",pos);
        msg.put("COLOR",playerID);
        msg.put("CurrentPos", ludo.getPosition(playerID,pieceID));
        msg.put("PIECE", pieceID);

        ClientConnection.getConnection().sendMSG(msg);
    }

    /**
     * Call this to update the placers piece position.
     * In our mind, the playerStructure is:
     * RED 0, 1, 2, 3
     * BLUE 4,5,6,7
     * YELLOW 8, 9, 10, 11
     * GREEN 12, 13, 14, 15
     *
     * This means that the first player is 0, and first piece is 0
     * However, the first piece is 1** - so we add one to piece.
     *
     * @param player
     * @param piece
     * @param pos
     */
    private void relocatePiece(int player, int piece, int pos){
        int offset = 10;
        piece+=1;
        Point newPosition = BoardPlacementToCord(pos);
        if (newPosition == null) return;
        anchor.getChildren().get(player*4+piece).relocate(newPosition.getX() + offset, newPosition.getY() + offset);

    }


    int BoardSize = 93;
    int fieldSize = 48;
    Point[] point;

    private int initialiseStartField(int count, int firstX, int firstY) {
        int i = count;
        point[i++].setLocation(firstX, firstY);
        point[i++].setLocation(firstX + fieldSize, firstY + fieldSize);
        point[i++].setLocation(firstX - fieldSize, firstY + fieldSize );
        point[i++].setLocation(firstX, firstY + fieldSize * 2);
        return i;
    }


    /**
     * Translates x and y cordinates from the screen til the correct boardID.
     * @param x
     * @param y
     * @return -1 or the fieldID
     */
    public int XYToBoardPlacement(int x, int y){
        int count = 0;
        for(Point p : point ){
            if((x >= p.getX() && x < p.getX()+48) && (y >= p.getY() && y < p.getY()+48))	return count;
            count++;
        }

        return -1;
    }

    /**
     * Translates a boardPlasment to cordinates.
     * @param placement - the tile number seen from the BoardGrid.
     * @return point.
     */
    public Point BoardPlacementToCord(int placement){
        if(placement < point.length){
            return point[placement];
        }
        return null;
    }

    /**
     * A function to find the cordinates of the tiles.
     *
     * The idea where to do it in a more generic way than the hardcoding done at:
     * https://bitbucket.org/okolloen/imt3281-ludo/src/f5882337e758c640dd97828d7c1fe046d00c7af5/
     * src/no/hig/imt3281/ludo/client/LudoBoard.java?at=master&fileviewer=file-view-default
     */
    public void TopLeftCorner() {
        point = new Point[BoardSize];
        for (int i = 0; i < BoardSize; i++) {
            point[i] = new Point();
        }

        int fieldSize = 48;

        /**
         * Red start
         */
        int firstX = 550;
        int firstY = 72;
        int i = 0;
        i = initialiseStartField(i, firstX, firstY);

        /**
         * Blue start
         */
        firstY = 507;
        i = initialiseStartField(i, firstX, firstY);

        /**
         * yellow start
         */
        firstX = 120;
        i = initialiseStartField(i, firstX, firstY);

        /**
         * Green start
         */
        firstY = 72;
        i = initialiseStartField(i, firstX, firstY);

        /**
         * 16 - 67
         */
        firstX = 383;
        firstY = 49;
        int maxPieces = 51;

        /**
         * Reds - 1 To 5
         */
        for (int num = 1; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstY += fieldSize;
        }
        /**
         * Jump over unused squere
         */
        firstX += fieldSize;

        /**
         * Reds - 6 to 10
         */
        for (int num = 0; num < 5; num++) {
            point[i++].setLocation(firstX, firstY);
            firstX += fieldSize;
        }

        /**
         * Reds 11 - 13
         */
        for (int num = 0; num < 3; num++) {
            point[i++].setLocation(firstX, firstY);
            firstY += fieldSize;
        }
        firstY -= fieldSize;
        firstX -= fieldSize;

        /**
         * Reds - 14 To 18
         */
        for (int num = 0; num < 5; num++) {
            point[i++].setLocation(firstX, firstY);
            firstX -= fieldSize;
        }
        /**
         * Jump over unused squere
         */
        firstY += fieldSize;

        /**
         * Reds 19 to 24
         */
        for (int num = 0; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstY += fieldSize;
        }
        firstY -= fieldSize;

        /**
         * 25/26
         */
        firstX -= 48;
        point[i++].setLocation(firstX, firstY);
        firstX -= 48;

        /**
         * Reds - 27 To 31
         */
        for (int num = 0; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstY -= fieldSize;
        }

        firstX -= fieldSize;

        /**
         * 32-40
         */
        for (int num = 0; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstX -= fieldSize;
        }
        firstX += fieldSize;
        firstY -= fieldSize;
        /**
         * 41/42
         */
        for (int num = 0; num < 2; num++) {
            point[i++].setLocation(firstX, firstY);
            firstY -= fieldSize;
        }
        firstY += fieldSize;
        firstX += fieldSize;
        /**
         * 43 - 47
         */
        for (int num = 0; num < 5; num++) {
            point[i++].setLocation(firstX, firstY);
            firstX += fieldSize;
        }

        firstY -= fieldSize;

        /**
         * 48 - 53
         */
        for (int num = 0; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstY -= fieldSize;
        }

        firstY += fieldSize;
        firstX += 48;
        point[i++].setLocation(firstX, firstY);
        firstX += 48;
        point[i++].setLocation(firstX, firstY);
        /**
         * Red winning line
         */
        firstX = 333;
        firstY = 41;
        for (int num = 0; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstY += fieldSize;
        }
        /**
         * blue winning line
         */
        firstX = 622;
        firstY = 335;
        for (int num = 0; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstX -= fieldSize;
        }
        /**
         * yellow winning line
         */
        firstX = 333;
        firstY = 620;
        for (int num = 0; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstY -= fieldSize;
        }
        /**
         * green winning line
         */
        firstX = 48;
        firstY = 336;
        for (int num = 0; num < 6; num++) {
            point[i++].setLocation(firstX, firstY);
            firstX += fieldSize;
        }

        for (int c = 0; c < BoardSize; c++)
            System.out.println(c + " " + point[c]);

    }
    /**
     * Setting the id of the game from server
     * @param gameId
     */
	public void setId(String gameId) {
		ludo.setId(gameId);
	}
	
	/**
	 * @return id of the game
	 */
	public String getId() {
		return ludo.getId();
	}
	/**
	 * Sets the number of the player owning the session and updating player names
	 * @param playerNumber
	 */
	public void setPlayerNumber(Integer playerNumber) {
		this.playerNumber = playerNumber;
		updatePlayerNames();
	}
	/**
	 * Updates the player names of the ludo game. called when a player joins/leaves/starts a game
	 */
	private void updatePlayerNames() {
		for(int i = 0; i <= 3; i++) 
			updatePlayerName(i);
		
	}
	/**
	 * Updating the player name in the game for a specific player
	 * @param playerNumber the player to update the name for
	 */
	private void updatePlayerName(int playerNumber) {
		Label player = player1Name;
		switch (playerNumber) {
			case 0:
				player = player1Name;
				break;
			case 1:
				player = player2Name;
				break;
			case 2:
				player = player3Name;
				break;
			case 3:
				player = player4Name;
				break;
		}
		
		player.setText(ludo.getPlayerName(playerNumber));
	}
	/**
	 * sets up the pane for the game
	 * @param gameBoard the pane
	 */
	public void setPane(AnchorPane gameBoard) {
		this.gameBoard = gameBoard;
		throwTheDice.setText(I18N.getBundle().getString("ludogameboard.wait"));
		throwTheDice.setDisable(true);
	}
	/**
	 * Shows the active player with the dice image next to name
	 * @param player
	 */
	private void showActivePlayer(int player) {
		ImageView[] playersActive = new ImageView[]{
				player1Active,
				player2Active,
				player3Active,
				player4Active};
		for (int i = 0; i < playersActive.length; i++) {
			if( i == player)
				playersActive[i].setVisible(true);
			else
				playersActive[i].setVisible(false);
		}
	}

    /**
     * Sets the chatID, so we can all know what chat is assosiated with this game.
     * @param chatID
     */
    public void setChatID(int chatID) {
        this.chatID = chatID;
    }

    /**
     * Send a message. Triggered when a user click on send.
     * @param actionEvent
     */
    public void sendMsg(ActionEvent actionEvent) {
        ClientConnection.sendText(textToSay.getText().toString(), "CHATMESSAGE", String.valueOf(this.chatID));
        textToSay.setText(null);
    }
}