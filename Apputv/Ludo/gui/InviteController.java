package no.ntnu.imt3281.ludo.gui;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.control.Label;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;

/**
 * InviteController - used for being invited to a game.
 */
public class InviteController {
	@FXML Label challengeLabel;

	private String inviter = "";
	private String gameId;
	private LudoController ludoController;
	private Tab tab;

	/**
	 * Set the message saying who invited you to the game.
	 */
	@FXML public void initialize() {
		challengeLabel.setText(inviter+ " " + I18N.getBundle().getString("invite.message"));
	}

	/**
	 * InviteController
	 */
	public InviteController() {
	}
	/**
	 * Sets the details regarding invite
	 * @param msg contains the details
	 */
	public void inviteDetails(Map<String, Object> msg) {
		challengeLabel.setText((String)msg.get("USERNAME") + " " + I18N.getBundle().getString("invite.message"));
		this.inviter = (String)msg.get("USERNAME");
		this.gameId = (String)msg.get("ID");
	}
	
	/**
	 * Accepting the invite, sending message to the server that the client want to join the game, closing the tab 
	 */
	@FXML
	public void acceptInvite() {
		ClientConnection.sendGameMessage("ACCEPTINVITE", "", gameId);
		ludoController.removeTab(tab);
	}
	/**
	 * Cancel the invite and remove the tab
	 */
	@FXML
	public void cancelInvite() {
		ludoController.removeTab(tab);
	}

	/**
	 * Setting the ludo controller, making it possible to access the controller for removing the tab
	 * @param ludoController that started this window
	 * @param tab that this window belongs to
	 */
	public void setLudoController(LudoController ludoController, Tab tab) {
		this.ludoController = ludoController;
		this.tab = tab;
	}
	
}
