package no.ntnu.imt3281.ludo.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.logic.GameListener;
/**
 * Leaderboard ranking controller
 * Displays the leaderboard ranking list of most games played and most won games.
 * @author Stian
 *
 */
public class RankingController {
	@FXML
	private ListView<String> wonList;
	@FXML
	private ListView<String> playedList;
	
	/**
	 * requesting top then lists from server
	 */
	@FXML protected void initialize() {
		GameListener rankingList = data -> Platform.runLater(()-> {
			updatePlayerWonList(data);
			updatePlayerGamesList(data);
		});
		ClientConnection.getConnection().addGameListner("RANKINGLIST", rankingList);

		ClientConnection.rankingListRequest();
	}
	/**
	 * Updates the games played ranking listView
	 * @param data
	 */
	private void updatePlayerGamesList(Map<String, Object> data) {
		ArrayList<String> players = (ArrayList<String>) data.get("PLAYEDLIST");
		ObservableList<String> playerList = FXCollections.observableArrayList();
		for(String player : players){
			playerList.add(player);
		}
		playedList.setItems(playerList);
	}
	/**
	 * Updates the won ranking listView
	 * @param data
	 */
	private void updatePlayerWonList(Map<String, Object> data) {
		ArrayList<String> players = (ArrayList<String>) data.get("WONLIST");
		ObservableList<String> playerList = FXCollections.observableArrayList();
		for(String player : players){
			playerList.add(player);
		}
		wonList.setItems(playerList);
	}
	
}
