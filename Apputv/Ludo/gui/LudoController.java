package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.client.Globals;

/**
 * Ludocontroller. - used as the main class for GUI related work.
 */
public class LudoController {
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);
	
    @FXML
    private MenuItem random;

    @FXML
    private TabPane tabbedPane;
    
    @FXML
    private Label loggedInMessage;
    


	/** ludoControllers constructure
	 * Set save the ludoController so we can use it to call functions when recieving events from the Server.
	 */
	public LudoController() {
	   ClientConnection.setLudoController(this);
   }

	/**
	 * Sets up the mainChat.
	 */
	@FXML public void newChat(){
   		Platform.runLater(()-> setupNewChat(1));
	}

	/**
	 * Created a new tab with the chat
	 * @param id
	 */
	public void setupNewChat(int id){
		FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"));
		loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
		ChatController controller;
		try {
			BorderPane gameBoard = loader.load();
			Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.chat"));
			tab.setContent(gameBoard);
			controller = loader.getController();
			controller.setID(String.valueOf(id));
			tabbedPane.getTabs().add(tab);
		} catch (IOException e1) {
			logger.throwing(this.getClass().getName(), "newChat", e1);
		}
	}

	/**
	 * Sets up the loginGUI, allowing the user to login.
	 */
    @FXML public void login(){
    try {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
		loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
		AnchorPane login = loader.load();
		LoginController controller = loader.getController();
		Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.login"));	
		controller.setLudoController(this, tab);
		tab.setContent(login);
		tabbedPane.getTabs().add(tab);
		tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));

	} catch (IOException e1) {
			logger.throwing(this.getClass().getName(), "login", e1);
		}

	}
    /**
     * Client tells the server that user wants to join a random game
     */
    @FXML
    public void joinRandomGame() {

    	if(!ClientConnection.isLoggedIn()) {
    		login();
    	} else {
	    	ClientConnection.sendGameMessage("RANDOMGAME", "", "-1");
    	}	
    }

	/**
	 * Create the register GUI, allowing the user to register a user.
	 */
	@FXML public void register(){
    	try {
	   		FXMLLoader loader = new FXMLLoader(getClass().getResource("register.fxml"));
	   		loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
	   			AnchorPane register = loader.load();
	   			LoginController controller = loader.getController();
	   			Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.register"));
	   			controller.setLudoController(this, tab);
	   			tab.setContent(register);
	   			tabbedPane.getTabs().add(tab);
	   			tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));
   		} catch (IOException e1) {
   			logger.throwing(this.getClass().getName(), "register", e1);
   		}

   	}

    /**
     * Creating a new game tab
     * @param msg gameId and players
     */
	public void createNewGame(Map<String, Object> msg) {
		Platform.runLater(() -> {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
			loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
	
			try {
				AnchorPane gameBoard = loader.load();
				
				GameBoardController controller = loader.getController();
				
				controller.setId(msg.get("ID").toString());
				controller.setPlayerNumber((Integer) msg.get("PLAYERSIZE"));
				controller.setPane(gameBoard);
				Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.game") + " " + msg.get("ID").toString());
				tab.setContent(gameBoard);
				tabbedPane.getTabs().add(tab);
				tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));
			} catch (IOException e) {
				logger.throwing(this.getClass().getName(), "createNewGame", e);
			}
		});
	}

	/**
	 * Opens up the challengePlayers GUI. Allowing the user to start a ludo game with some other players.
	 */
	@FXML public void challengePlayers() {
		if(!ClientConnection.isLoggedIn()) {
    		login();
    	} else {
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("challengePlayers.fxml"));
				loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
				AnchorPane challengePlayers = loader.load();
				ChallengePlayerController controller = loader.getController();
				
				Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.challenge"));
				controller.setLudoController(this, tab);
				
				tab.setContent(challengePlayers);
				tabbedPane.getTabs().add(tab);
				tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));
			} catch (IOException e1) {
				logger.throwing(this.getClass().getName(), "challengePlayers", e1);
			}
    	}
	}

	/**
	 * Opens up the joinRoom gui. Allowing the user to see all the rooms he can join.
	 */
	@FXML public void joinRoom() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("JoinRoom.fxml"));
			loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
			AnchorPane joinRoom = loader.load();
			//JoinRoomController controller = loader.getController();
			Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.joinRoom"));
			tab.setContent(joinRoom);
			tabbedPane.getTabs().add(tab);
			tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));
		} catch (IOException e1) {
			logger.throwing(this.getClass().getName(), "joinRoom", e1);
		}
	}

	/**
	 * Opens up the listRoom gui. Allowing the user to see all the rooms he is in.
	 */
	@FXML public void listRoom() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ListRoom.fxml"));
			loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
			AnchorPane listRoom = loader.load();
			//ListRoomController controller = loader.getController();
			Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.listRoom"));
			tab.setContent(listRoom);
			tabbedPane.getTabs().add(tab);
			tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));
		} catch (IOException e1) {
			logger.throwing(this.getClass().getName(), "listRoom", e1);
		}
	}

	/**
	 * Showing invite dialog
	 * @param msg starting the tab where the user can invite other players to a game
	 */
	public void invitePlayer(Map<String, Object> msg) {
		Platform.runLater(() -> {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("InviteController.fxml"));
			loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
			AnchorPane invitePane = loader.load();
			InviteController controller = loader.getController();
			
			Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.invite"));
			controller.setLudoController(this, tab);
			controller.inviteDetails(msg);
			
			tab.setContent(invitePane);
			tabbedPane.getTabs().add(tab);
			tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "invitePlayer", e);
		}
		});
	}

	/**
	 * View the userprofile of a user, should be called maybe when we click on a player in in game?
	 *
	 * @param username
	 */
	public void viewUserProfile(String username){
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("viewUserProfile.fxml"));
			loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
			AnchorPane invitePane = loader.load();
			ViewUserProfileController controller = loader.getController();
			controller.setID(username);
			Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.viewProfile"));
			tab.setContent(invitePane);
			tabbedPane.getTabs().add(tab);
			tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));

		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "viewUserProfile", e);
		}
	}

	/**
	 * Opens up the edituserProfile GUI. Allowing the user to edit his own profile.
	 */
	@FXML public void editUser() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("EditUserProfile.fxml"));
			loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
			AnchorPane invitePane = loader.load();
			Tab tab = new Tab(I18N.getBundle().getString("ludocontrollertab.editProfile"));

			tab.setContent(invitePane);
			tabbedPane.getTabs().add(tab);
			tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "editUser", e);
		}
	}

	/**
	 * Close the connection.
	 */
	@FXML public void closeButton() {
		ClientConnection.stopConnection();
		Stage stage = (Stage) loggedInMessage.getScene().getWindow();
		stage.close();

	}
	
	/**
	 * Setting the logged in message with the logged in username
	 * @param username the username to have in the message
	 */
	public void setLoginMessage(String username, String token) {
		Platform.runLater(() -> {
			if("TOKENFAILURE".equals(token)) {
				this.loggedInMessage.setText("");
			} else {
				this.loggedInMessage.setText(I18N.getBundle().getString("ludocontroller.loggedInMes") + " " + username);
			}
		});
		
	}

	/**
	 * Remove a given tab from the view.
	 * @param tab
	 */
	public void removeTab(Tab tab) {
		tabbedPane.getTabs().remove(tab);
	}
	/**
	 * Opens up the ranking lists GUI.
	 */
	@FXML public void ranking() {	
		if(!ClientConnection.isLoggedIn()) {
    		login();
    	} else {
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("ranking.fxml"));
				loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
				AnchorPane ranking = loader.load();
				//RankingController controller = loader.getController();
			
				Tab tab = new Tab(I18N.getBundle().getString("ludo.ranking"));
				
				tab.setContent(ranking);
				tabbedPane.getTabs().add(tab);
				tabbedPane.getSelectionModel().select(tabbedPane.getTabs().indexOf(tab));
				
			} catch (IOException e) {
				logger.throwing(this.getClass().getName(), "ranking", e);
			}
    	}
	
	}
}
