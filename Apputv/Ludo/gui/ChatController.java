package no.ntnu.imt3281.ludo.gui;


import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.logic.GameListener;

import java.util.ArrayList;
import java.util.Map;


/**
 *
 * This is the main class for the chat.
 * It handles incomming chat message and allows the user to send messages.
 *
 * @author 
 *
 */
public class ChatController {
    private String ID = "1";
    @FXML
    private TextField chatTextField;
    @FXML
    private TextArea chatTextArea;
    @FXML
    private ListView<String> list;


    public void setID(String id){
        this.ID = id;
    }
    public String getID(){
        return ID;
    }

    /**
     * Sends the text which have been typed into the chat text field
     * when the user presses enter,
     * Then set the cat text field to be empty agaun
     *
     * @param  event  A keyevent which checks on enter from the user.
     */
    @FXML
    void sendChat(KeyEvent event){
        if(event.getCode() == KeyCode.ENTER && chatTextField.getText() != null) {               //Sende med brukertoken
            ClientConnection.sendText(chatTextField.getText().toString(), "CHATMESSAGE",this.ID);
            chatTextField.setText(null);
        }
    }

    /**
     * Listens for chat messages. Recieves them and adds them to the chat text area
     * with user name and message for the right group.
     *
     */
    @FXML
    protected void initialize() {

        GameListener chatmsg = data -> Platform.runLater(()->updateChat(data));
        ClientConnection.getConnection().addGameListner("CHATMESSAGEREPLY",chatmsg);

        GameListener playerlistUpdate = data -> Platform.runLater(()->updatePlayerList(data));
        ClientConnection.getConnection().addGameListner("PLAYERLISTFORCHANNEL",playerlistUpdate);

        ClientConnection.playerListRequest(Integer.parseInt(this.getID()));

        /**
         * Setup a mini menu for selecting viewUserProfile and startChat
         */
        ContextMenu cm = new ContextMenu();
        MenuItem newChat = new MenuItem("Start chat");
        cm.getItems().add(newChat);
        MenuItem viewProfile = new MenuItem("View userprofile");
        cm.getItems().add(viewProfile);

        newChat.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String data = list.getSelectionModel().getSelectedItem();
                ClientConnection.sendNewChatMessage(data, "NEWCHAT");            }
        });

        viewProfile.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String user = list.getSelectionModel().getSelectedItem();
                ClientConnection.getConnection().viewProfileFor(user);
            }
        });

        list.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent t) {
                if(t.getButton() == MouseButton.PRIMARY)
                {
                    cm.show(list , t.getScreenX() , t.getScreenY());
                }
            }
        });
    }

    /**
     * Updates the playerList about who is in the channel
     * @param data
     */
    private void updatePlayerList(Map<String, Object> data) {
        if(this.getID().equals(String.valueOf((int)data.get("GROUPID")))) {

            ArrayList<String> users = (ArrayList<String>) data.get("CLIENTLIST");
            ObservableList<String> FriendList = FXCollections.observableArrayList();
            for (String user : users) {
                FriendList.add(user);
            }
            list.setItems(FriendList);
        }
    }

    /**
     * Updates the chat when a new message come.
     * @param data
     */
    private void updateChat(Map<String, Object> data) {
        if(String.valueOf((int)data.get("GROUPID")).equals(this.ID)) {
            chatTextArea.setWrapText(true);
            chatTextArea.appendText(data.get("USERNAMEOFSENDER") + ": " + data.get("MESSAGE") + "\n");
        }
    }


}