package no.ntnu.imt3281.ludo.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.client.Globals;
import no.ntnu.imt3281.ludo.logic.GameListener;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * View a userprofile for a given user.
 * This opens a gui showing them info: Name, BIO, and profilePicture.
 */
public class ViewUserProfileController {
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);
	
    @FXML TextField txtUsername;
    @FXML TextField txtBio;
    @FXML private ImageView profilePicture;
    @FXML Label Username;
    @FXML Label BioText;
    String name;


    /**
     * initialises the name, and sets up the userinfo listner.
     * @param name
     */
    public void setID(String name){
        this.name = name;

        GameListener userinfo = data -> Platform.runLater(()->userinfoHandler(data));
        ClientConnection.getConnection().addGameListner("USERINFO",userinfo);

        ClientConnection.getConnection().getUserInfo(this.name);

    }

    /**
     * Update the userinfo, based on the userinfo we get.
     * @param data
     */
    private void userinfoHandler(Map<String, Object> data) {
        if(!this.name.equals(data.get("USERNAME"))) return;

        Username.setText("Username: " + (String) data.get("USERNAME"));
        BioText.setText("BIO: " + (String) data.get("BIO"));

        /**
         * Read in and set the image, if a user have a profile picture.
         */
        if(data.get("IMAGE") != null) {

            Object obj = data.get("IMAGE");
            File file = new File("tempImg");
            try {
                OutputStream os = new FileOutputStream(file);
                os.write((byte[]) obj);
                os.close();
            } catch (IOException e) {
                logger.throwing(this.getClass().getName(), "setID", e);
            }

            Image image = new Image(file.toURI().toString());
            profilePicture.setImage(image);
        }

    }



}
