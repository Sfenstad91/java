package no.ntnu.imt3281.ludo.gui;



import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.logic.GameListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * This is the main class for the join room.
 * It shows the user a list of available rooms
 * And provides the ability to join one of the rooms
 *
 * @author Odd
 *
 */
public class JoinRoomController {
    @FXML private ListView<String> list;

    /**
     * Set up a listner for new channellist requests and request a new channellist
     */
    @FXML
    protected void initialize() {

        GameListener channellist = data -> Platform.runLater(()-> handleNewChannelList(data));
        ClientConnection.getConnection().addGameListner("CHANNELLIST",channellist);

        ClientConnection.channelListRequest(false);


    }

    /**
     * Parses the Channellist and updates the array
     * @param data
     */
    private void handleNewChannelList(Map<String, Object> data) {
        ArrayList<Integer> channels = (ArrayList<Integer>) data.get("CHANNELLIST");
        if(channels == null) return;
        ObservableList<String> ClientList = FXCollections.observableArrayList();
        for(Integer channelID: channels){
            ClientList.add(channelID.toString());
        }
        list.setItems(ClientList);
    }

    /**
     * The join button
     * Sends the groupid as a string
     */
    public void nowJoinRoom() {
        String joinString = list.getSelectionModel().getSelectedItem();
        ClientConnection.sendJoinMessage(joinString, "JOINCHAT");
        ClientConnection.channelListRequest(false);
    }
}
