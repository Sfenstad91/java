package no.ntnu.imt3281.ludo.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.logic.GameListener;
import javafx.scene.control.Tab;

/**
 * Channelenge a player to a game of ludo.
 */
public class ChallengePlayerController {
	@FXML
	private ListView<String> list;
	private ArrayList<String> challenges = new ArrayList<>();
	private LudoController ludoController;
	private Tab tab;
	
	/**
	 * requesting list of players from server
	 */
	@FXML
	protected void initialize() {
		GameListener playerlist = data -> Platform.runLater(()->updatePlayerList(data));
		ClientConnection.getConnection().addGameListner("PLAYERLIST",playerlist);

		ClientConnection.playerListRequest();

	}

	/**
	 * Updates the playerListView
	 * @param data
	 */
	private void updatePlayerList(Map<String, Object> data) {
		ArrayList<String> users = (ArrayList<String>) data.get("CLIENTLIST");
		ObservableList<String> ClientList = FXCollections.observableArrayList();
		for(String user: users){
			ClientList.add(user);
		}
		list.setItems(ClientList);
	}

	/**
	 * Adding the clicked player to the invite list, removing the player from the gui
	 * starting game if there is 3 invites
	 */
	@FXML
	public void challengePlayer() {
		challenges.add(list.getSelectionModel().getSelectedItem());
		list.getItems().remove(list.getSelectionModel().getSelectedItem());
		if (challenges.size() >= 3)
			createGame();
	}
	/**
	 * The create game button or when user has invited four players
	 */
	public void createGame() {
		StringBuilder sb = new StringBuilder();
		Iterator<String> itr = challenges.iterator();
		while(itr.hasNext()) {
			sb.append(itr.next());
			sb.append(" ");
		}
		ClientConnection.sendGameMessage("PRIVATEGAME", sb.toString(), "-1");
		ludoController.removeTab(tab);
	}
	
	/**
	 * Setting the ludo controller, making it possible to access the controller for removing the tab
	 * @param ludoController  that started this window
	 * @param tab that this window belongs to
	 */
	public void setLudoController(LudoController ludoController, Tab tab) {
		this.ludoController = ludoController;
		this.tab = tab;
	}
}
