package no.ntnu.imt3281.ludo.gui;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.logic.IllegalPlayerNameException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;

/**
 * Logincontroller. Used for login and Regiser.
 */
public class LoginController{
    ClientConnection con = null;
    
    @FXML
    private Button loginB;
    
    @FXML 
    private Button regB;

    @FXML
    private Label isConnected;

    @FXML
    private TextField txtUsername;

    @FXML
    private PasswordField txtPassword;
    
    @FXML
    private Label infoLabel;
    
    @FXML
    private Label infoLabelr;
    
     @FXML
    private CheckBox checkBox;
     
    private LudoController ludoController;
    
    private Tab tab;


	/**
	 * empty constuctor...
	 */
	public LoginController() {
    }

	/**
	 * Login a user. Send the request to the server for validation after some clientside validation.
	 * @param actionEvent
	 */
	@FXML public void login(ActionEvent actionEvent){
    	if(txtUsername.getText().length() > 1 && txtPassword.getText().length() > 1) {
			ClientConnection.setLoginController(this);
            ClientConnection.getConnection().sendSrvMsg("LOGIN",txtUsername.getText().toLowerCase(),txtPassword.getText());
	    } else {
	    	infoLabel.setText(I18N.getBundle().getString("login.loginShort"));
	    }
    }

	/**
	 * Register. Send the register event with the username and password to the server.
	 * After doing clientside validation of length and name.
	 * @param actionEvent
	 * @throws IllegalPlayerNameException
	 */
	@FXML public void register(ActionEvent actionEvent) throws IllegalPlayerNameException{
    	if (txtUsername.getText().length() > 1 && txtPassword.getText().length() > 1) {
    		if(!txtUsername.getText().startsWith("****")) {
    			ClientConnection.setLoginController(this);
	    		ClientConnection.getConnection().sendSrvMsg("REGISTER",txtUsername.getText().toLowerCase(),txtPassword.getText());
    		} else 
    			throw new IllegalPlayerNameException(I18N.getBundle().getString("loginregister.invalidUsername"));
	    } else {
	    	infoLabelr.setText(I18N.getBundle().getString("login.loginShort"));
	    }
   }

	/**
	 * Register response handler.
	 * Show the user that they successfully registered the user. Or show error if not.
	 * @param token
	 */
	public void registerResponse(String token) {
    	Platform.runLater(() -> {
	    	if (token.equals("TOKENFAILURE")) {	
	    		infoLabelr.setText(I18N.getBundle().getString("login.notRegistered"));
	    	} else {
	    		infoLabelr.setText(I18N.getBundle().getString("login.success"));
				ludoController.removeTab(tab);
	    	}
    	});
    }

	/**
	 * Login response. Show the user that the login was successfull, or if it was not.
	 * @param token
	 */
	public void loginResponse(String token) {
    	Platform.runLater(() -> {
	    	if(token.equals("TOKENFAILURE")) {    		
	    		infoLabel.setText(I18N.getBundle().getString("login.wrongUserPass"));
	    	} else {
	    		infoLabel.setText(I18N.getBundle().getString("login.success"));
	    		ludoController.removeTab(tab);
	    	}
    	});
    }
    /**
	 * Setting the ludo controller, making it possible to access the controller for removing the tab
	 */
	public void setLudoController(LudoController ludoController, Tab tab) {
		this.ludoController = ludoController;
		this.tab = tab;
	}

	/**
	 * check if checkbox is selected.
	 * @return true if it is.
	 */
	public boolean getCheckBox() {
		return this.checkBox.isSelected();
	}
}
