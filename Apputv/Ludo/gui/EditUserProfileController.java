package no.ntnu.imt3281.ludo.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.client.Globals;
import no.ntnu.imt3281.ludo.logic.GameListener;
import no.ntnu.imt3281.ludo.logic.IllegalPlayerNameException;
import java.io.File;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Edit userprofile.
 */
public class EditUserProfileController {
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);
	
    @FXML TextField txtUsername;
    @FXML TextField txtBio;
    @FXML private ImageView profilePicture;
    @FXML PasswordField txtPassword1;
    @FXML PasswordField txtPassword;
    File pictureFile;
    BufferedImage img = null;
    byte[] fileContent;
    @FXML
    Label errorLabel;

    /**
     * Allow the user to select a new profile image.
     */
    @FXML protected void locateFile() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle(I18N.getBundle().getString("edituser.selectPic"));
        File file = chooser.showOpenDialog(new Stage());
        this.pictureFile = file;
        try {
            this.fileContent = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            logger.throwing(this.getClass().getName(), "locateFile", e);
        }

        Image image = new Image(file.toURI().toString());
        profilePicture.setImage(image);

        HashMap<String,Object> userdata = new HashMap<>();
        userdata.put("TYPE", "UPDATEPROFILEPIC");
        userdata.put("IMAGE", this.fileContent);
        ClientConnection.getConnection().sendMSG(userdata);

    }

    /**
     * should tell the server about the new edit we want to do
     * @throws IllegalPlayerNameException
     */

    public void edit() throws IllegalPlayerNameException {
    	if(!txtUsername.getText().startsWith("****")) {
    		 HashMap<String,Object> userdata = new HashMap<>();
	        boolean passwordIsSimilar = txtPassword.getText().equals(txtPassword1.getText());
	        boolean passwordIsEmpty = "".equals(txtPassword.getText()) || "".equals(txtPassword1.getText());
	
	        userdata.put("TYPE", "UPDATEUSERINFO");
	        userdata.put("USERNAME", txtUsername.getText());
	        userdata.put("BIO", txtBio.getText());
	
	        if(!passwordIsEmpty && passwordIsSimilar){
	            userdata.put("PASSWORD", txtPassword.getText());
	        }

            Platform.runLater(()->{
                errorLabel.setVisible(false);
                ClientConnection.getConnection().sendMSG(userdata);
                ClientConnection.getConnection().getUserInfo("");

            });

    	} else {
            Platform.runLater(()-> errorLabel.setVisible(true));
    	}
       
    }

    /**
     * Initialize the listners
     */
    @FXML protected void initialize() {

        GameListener userinfo = data -> Platform.runLater(()-> userinfoHandler(data));
        ClientConnection.getConnection().addGameListner("USERINFO",userinfo);
        /**
         * Ask the server to get up to date info, and then we will have the listner notify us when we got the reply
         */
        ClientConnection.getConnection().getUserInfo("");

        GameListener wrongInput = data -> Platform.runLater(()-> wrongInput());
        ClientConnection.getConnection().addGameListner("EDITUSERTAKEN",wrongInput);

    }

    /**
     * Tells the user that the update got denied and it was because to data was wrong
     */
    private void wrongInput() {
            errorLabel.setVisible(true);
    }

    /**
     * Updates the view with the users info.
     * @param data
     */
    private void userinfoHandler(Map<String, Object> data) {
        String username = ClientConnection.getConnection().getUserName();
        if(!username.equals((String)data.get("USERNAME"))) return;
        // Now here we got access to the up to date msg obj.
        txtUsername.setText((String) data.get("USERNAME"));
        txtBio.setText((String) data.get("BIO"));

        // Read in and set the image, if a user have a profile picture.
        if (data.get("IMAGE") != null) {

            Object obj = data.get("IMAGE");
            File file = new File("tempImg");
            try {
                OutputStream os = new FileOutputStream(file);
                os.write((byte[]) obj);
                os.close();
            } catch (IOException e) {
                logger.throwing(this.getClass().getName(), "initialize", e);
            }

            Image image = new Image(file.toURI().toString());
            profilePicture.setImage(image);
        }
    }
}
