package no.ntnu.imt3281.ludo.server;

import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.client.Globals;
/**
 * Class to handle timeout when a user is afk(Away from keyboard) for too long.
 * @author Cim
 *
 */
public class TreadTimeout extends Thread{
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);
	
    Game game;
    int timeout;
    int oldEventID;
    /**
     * Constructor for the class TreadTimeout. It takes and game object and a integer of time.
     * @param game
     * @param timeoutInSeconds
     */
    public TreadTimeout(Game game, int timeoutInSeconds) {
        this.game = game;
        this.timeout = timeoutInSeconds;
    }
    
    /**
     * Run method for the class TreadTimeout. This method handles the timeouts.
     */
    @Override
    public void run(){
        oldEventID = game.geteventNr();
        int  neweventID;
        while(!"FINISHED".equals(game.getStatus())){
            try {
                Thread.sleep(this.timeout*(long)1000);
            } catch (InterruptedException e) {
                logger.throwing(this.getClass().getName(), "run", e);
                Thread.currentThread().interrupt();
            }
            neweventID = game.geteventNr();
            if(oldEventID == neweventID){
                /**
                 * The user have not done any moves for timeout time
                 * Set him to Inactive
                 */
                game.playerIsInactive();
            }
            oldEventID = neweventID;
        }
    }
}
