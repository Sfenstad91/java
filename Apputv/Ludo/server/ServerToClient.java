package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.client.Globals;
import no.ntnu.imt3281.ludo.logic.GameListener;
import no.ntnu.imt3281.ludo.logic.UserData;

import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Logger;


/**
 * This class contains the socket listening to clients.
 * @author
 *
 */
public class ServerToClient extends Thread{
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);
	
	private ObjectOutputStream OS = null;
	private ObjectInputStream is;

	private String username;
	DatabaseTableModel database;
	Server server = null;
	UserData User = new UserData();
	private HashMap<String,ArrayList<GameListener>> Listners = new HashMap<>();
	private Instant lastPingTime;

	/**
    * sets up the input/output stream
     */
	public ServerToClient(Socket socket, Server serv) {
		try {
			this.server = serv;


			this.OS = new ObjectOutputStream(socket.getOutputStream());
			this.is = new ObjectInputStream(socket.getInputStream());


		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "ServerToClient", e);
		}

	}


	/**
	 * At regular times, check for a new message. now it sleeps 1 second.
	 */
	public void run() {
		while (true){

				CheckForNewMsg();

			try {
				sleep(1000);
			} catch (InterruptedException e) {
				logger.throwing(this.getClass().getName(), "run", e);
				Thread.currentThread().interrupt();
			}
		}
	}

	/**
	 * Check for new msg.
	 * Will stall at is.readObject() until a object is read.
	 * When a object is read, it delegates it to the correct handler.
	 * And throwEventForGameListner if someone subscribe to that type.
	 */
	private void CheckForNewMsg(){
		try {
			HashMap<String, Object> msg = (HashMap<String, Object>) is.readObject();
			switch((String)msg.get("TYPE")){
				case "REGISTER":
					registerHandler(msg);
					break;
				case "LOGIN":
					loginHandlerHash(msg);
					break;
				case "PLAYERLIST":
					getActiveClientHandler(msg);
					break;
				case "PLAYERLISTCHANNEL":
					getActiveClientInChannel(msg);
					break;
				case "PRIVATEGAME":
					privateGame(msg);
					break;
				case "CHATMESSAGE":
					chatMsgHandler(msg);
					break;
				case "NEWCHAT":
					newchatHandler(msg);
					break;
				case "GETUSERINFO":
					getInfo(msg);
					break;
				case "UPDATEUSERINFO":
					updateUserInfo(msg);
					break;
				case "UPDATEPROFILEPIC":
					updateUserImg(msg);
					break;
				case "ACCEPTINVITE":
					acceptInvite(msg);
					break;
				case "CHANNELLIST":
					channelListHandler(msg);
					break;
				case "CHANNELLISTMEMBER":
					channelListHandler(msg);
					break;
				case "JOINCHAT":
					joinChat(msg);
					break;
				case "RANDOMGAME":
					randomGame(msg);
					break;
				case "PING":
					pingHandler(msg);
					break;
				case "AUTOLOGIN":
					autoLoginHandler(msg);
					break;
				case "RANKINGLIST":
					getRankingList(msg);
					break;
				case "DISCONNECTED":
					server.clientDisconnect(getUsername());
					break;
			}
			throwEventForGameListner(msg);

		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "CheckForNewMsg", e);
		} catch (ClassNotFoundException e) {
			logger.throwing(this.getClass().getName(), "CheckForNewMsg", e);
		} catch (SQLException e) {
			logger.throwing(this.getClass().getName(), "CheckForNewMsg", e);
		}
	}
	/**
	 * Method that writes top 10 lists to a client who requested it
	 * @param conMsg message recieved from client
	 */
	private void getRankingList(HashMap<String, Object> conMsg) {
		HashMap<String, Object> msg = new HashMap<String, Object>();
		msg.put("TYPE",((String)conMsg.get("TYPE")));
		msg.put("WONLIST", database.getWonRanking());
		msg.put("PLAYEDLIST", database.getPlayedRanking());
		try {
			this.OS.writeObject(msg);

		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "getRankingList", e);
		}
	}


	/**
	 * Gets the username for all the active users in a given channel.
	 * @param msg - hashMap<String,Object>
	 */
	private void getActiveClientInChannel(HashMap<String, Object> msg) {
		HashMap<String, Object> returnmsg = new HashMap<>();

		int groupID = (int)msg.get("GROUPID");
		int userID = this.getUserID();
		ArrayList<String> users = server.getUserNameInGroup(groupID,userID);

		if(users == null) return;

		returnmsg.put("TYPE","PLAYERLISTFORCHANNEL");
		returnmsg.put("CLIENTLIST", users);
		returnmsg.put("GROUPID",groupID);

		//sendMessage(returnmsg);
		server.sendMsgToChannel(groupID,returnmsg);
	}

	/**
	 * Handle pings from the client.
	 * The client sends us, the server a ping request saying he is alive.
	 * We store the last ping time, so when its his turn in a game, we can check if he
	 * have lost his connection
	 * @param msg
	 */
	private void pingHandler(HashMap<String, Object> msg) {
		this.lastPingTime = Instant.now();
		System.out.println("Got new ping");
	}

	/**
	 * Get the timedate for the last time the client pinged us, telling us he where alive.
	 * @return Instant, with the time of the last ping.
	 */
	public Instant getlastPingTime(){
		return this.lastPingTime;
	}

	/**
	 * Tell the server the client wants to join a randomGame.
	 * @param msg
	 */
	private void randomGame(HashMap<String, Object> msg) {
		server.joinRandomGame(msg, this);
	}


	/**
	 * Throw an event to the Game listners listning to that type.
	 * @param msg
	 */
	private void throwEventForGameListner(HashMap<String,Object> msg) {
		String type = (String)msg.get("TYPE");
		ArrayList<GameListener> ListnerList = Listners.get(type);
		if(ListnerList != null) {
			for (GameListener listner :ListnerList)
				listner.GameChange(msg);
		}
	}

	/**
	 * Get a channel List, either where the user is a member, or where he is not.
	 * @param conmsg
	 */
	private void channelListHandler(HashMap<String, Object> conmsg) {
		boolean isMember = "CHANNELLISTMEMBER".equals((String)conmsg.get("TYPE"));
		HashMap<String, Object> msg = new HashMap<>();
		msg.put("TYPE",((String)conmsg.get("TYPE")));
		int userID = server.getClientIDFromPlayer(this.getUsername());
		msg.put("CHANNELLIST", server.getChannelsWhere(userID, isMember));
		try {
			this.OS.writeObject(msg);

		} catch (IOException e) {

			logger.throwing(this.getClass().getName(), "channelListHandler", e);
		}
	}


	/**
	 * insert the listner to a global map, so we can call the correct listner based on the key.
	 * @param key
	 * @param listner
	 */
	public void addGameListner(String key, GameListener listner) {
		ArrayList<GameListener> list = Listners.get(key);
		if(list == null) list = new ArrayList<>();

		list.add(listner);
		Listners.put(key,list);
	}

	/**
	 * Ask the server to create a new chat
	 * with the user and the other user that has been chosen
	 * Uses the username to get token, then sends both tokens.
	 *
	 * @param  msg  contains TOKEN, USERNAME.
	 *
	 */
	private void newchatHandler(HashMap<String, Object> msg) throws SQLException {
		String username = (String)msg.get("USERNAME");
		int groupID = server.makeGroupWith((int)this.getUserID(),
				server.getClientIDFromPlayer(username));
		/**
		 * Now that we have created the group, we need to tell
		 * the users that they are added to a group, with this ID
		 */
		notifyOfGroupCreating(groupID);

	}

	/**
	 * Notify all the members of the group, that the group have been created
	 * @param groupID
	 */
	private void notifyOfGroupCreating(int groupID){
		HashMap<String, Object> msg = new HashMap<>();
		msg.put("TYPE", "NEWCHATNOTIFY");
		msg.put("GROUPID", groupID);

		server.sendMsgToChannel(groupID,msg);
	}

	/**
	 * Tell the Client that he joined a group.
	 * @param groupID
	 */
	private void notifyOfJoiningGroup(int groupID){
		HashMap<String, Object> msg = new HashMap<>();
		msg.put("TYPE", "NEWCHATNOTIFY");
		msg.put("GROUPID", groupID);
		sendMessage(msg);
	}

	/**
	 * Ask the server to join a chat
	 * Sends token and groupid
	 *
	 * @param  msg  contains TOKEN, GROUPID.
	 *
	 */
	private void joinChat(HashMap<String, Object> msg) {
		int groupID = Integer.parseInt((String)msg.get("GROUPID"));
		if(server.addUserToGroup(this.getUserID(),groupID))
			notifyOfJoiningGroup(groupID);
	}

	/**
	 * Handles chat messages. If the user is a member in the group, send the message to the group.
	 * Ang log the message.
	 * @param msg
	 */
	private void chatMsgHandler(HashMap<String, Object> msg) {
		int groupID = Integer.parseInt((String)msg.get("GROUPID"));

		if(msg.get("TOKEN") == null || !this.User.getToken().equals(msg.get("TOKEN"))) return; // error - wrong token

		if(!server.isUserInChannel(this.getUserID(),groupID )) return; // error - wrong group

		server.pushNewMessageToGroup(groupID,(String)msg.get("MESSAGE"), this.User.getUserName());
		database.logChatMessages(this.User.getUserName(), (String)msg.get("MESSAGE"), groupID);
	}

	/**
	 * Updates the profilePicture stored in the database.
	 * @param map
	 */
	private void updateUserImg(HashMap<String, Object> map){
		if(map.get("TOKEN").equals(this.getToken())) {
			database.updateUserPicture(map);
		}
	}

	/**
	 * Updates the userInfo stored in the database.
	 * @param map
	 */
	private void updateUserInfo(HashMap<String, Object> map) {
		boolean userExist = database.isUserTaken((String)map.get("USERNAME"));
		if(!userExist || (userExist && this.getUsername().equals((String)map.get("USERNAME")))) {
			if(map.get("TOKEN").equals(this.getToken())) // Ehh. Should do this against the DB
			{
				database.editUserInfo(map);
			}
		} else {
			HashMap<String, Object> msg = new HashMap<String, Object>();
			msg.put("TYPE", "EDITUSERTAKEN");
			try {
				this.OS.writeObject(msg);
			} catch (IOException e) {
				logger.throwing(this.getClass().getName(), "updateUserInfo", e);
			}
		}
			
			

	}


	/**
	 * Send back userinfo to the client. In a HashMap. However, this function should take user as a arg, so we can use
	 * it for all users. As we will when we get ViewUserProfile()
	 */
	private void getInfo(HashMap<String,Object> msg){
		String name = (String)msg.get("USERNAME");
		if(name == null)	return;
		Object obj = database.getInfo(name);
		if(obj == null) return; // error;

		// Before sending we should update the type FIELD, so its compatible for when we no longer uses the msg interface.
		HashMap<String, Object> map = (HashMap<String, Object>)obj;
		map.put("TYPE", "USERINFO");



		try {
			this.OS.writeObject(obj);

		} catch (IOException e) {
			// If an error actually happens, the client would stall as it waits until it actually get a object.. We should handle that.

			logger.throwing(this.getClass().getName(), "getInfo", e);
		}
	}
	/**
	 * Method that writes all active clients/players online to client who wants to challenge players
	 * @param conMsg message recieved from client
	 */
	private void getActiveClientHandler(HashMap<String, Object> conMsg) {
		HashMap<String, Object> msg = new HashMap<String, Object>();
		msg.put("TYPE",((String)conMsg.get("TYPE")));
		msg.put("CLIENTLIST", server.getActiveClients(conMsg));
		try {
			this.OS.writeObject(msg);

		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "getActiveClienthandler", e);
		}
	}

	/**
	 * Starts a private game.
	 * @param conMsg
	 */
	private void privateGame(HashMap<String, Object> conMsg) {
		server.createPrivateGame(conMsg, this);
	}

	/**
	 * User accepted the invite to a game.
	 * @param msg
	 */
	private void acceptInvite(HashMap<String, Object> msg) {
		server.acceptInvite(msg, this);
		
	}

	private void loginHandlerHash(HashMap<String, Object> msg) {
		System.out.println("Checking for user " + msg.get("USERNAME") + " in database");
		String userToken = "TOKENFAILURE";
		try {

			database = new DatabaseTableModel();
			if(database.isLoggedIn((String)msg.get("USERNAME"), (String)msg.get("PASSWORD"))) {
				HashMap<String, Object> user = database.getInfo((String)msg.get("USERNAME"));

				System.out.println("Welcome " +msg.get("USERNAME"));

				userToken = database.getToken((String)msg.get("USERNAME"));
				this.User.setToken(userToken);

				setLoggedIn((String)msg.get("USERNAME"), userToken, (int)user.get("ID"));

				send("LOGINREPLY", userToken, (String)msg.get("USERNAME"));
			} else {
				send("LOGINREPLY", userToken, (String)msg.get("USERNAME"));
			}
		}catch (Exception e){
			logger.throwing(this.getClass().getName(), "loginHandlerHash", e);
		}
	}


	private void send(String keyword, String userToken, String userName) {
		HashMap<String, Object> msg = new HashMap<String, Object>();
		msg.put("TYPE", keyword);
		msg.put("TOKEN", userToken);
		msg.put("USERNAME", userName);
		msg.put("isLoggedIn", true);
		msg.put("ID", getUserID());

		try {
			this.OS.writeObject(msg);
			this.OS.flush();

		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "send", e);
		}
	}
	/**
	 * Method to handle the register request from client. This adds the usertoken, username and hashed(and salted)
	 * password to the database.
	 * @param msg containing username, and password in the stream.
	 */
	private void registerHandler(HashMap<String, Object> msg) {

		System.out.println("Adding user " + msg.get("USERNAME") + " to database");
		String userToken = "TOKENFAILURE";
		try {
			database = new DatabaseTableModel();
			if(!database.isUserTaken((String)msg.get("USERNAME"))) {
				String genToken = generateToken();
				database.addUser((String)msg.get("USERNAME"), (String)msg.get("PASSWORD"), genToken);
				System.out.println("Successfuly added user to database");
				userToken = genToken;

				HashMap<String, Object> user = database.getInfo((String)msg.get("USERNAME"));
				setLoggedIn((String)msg.get("USERNAME"), userToken, (int)user.get("ID"));
				send("REGISTERREPLY", userToken,  (String)msg.get("USERNAME") );
			} else {
				System.out.println("Username allready taken, try again");
				send("REGISTERREPLY", userToken, (String)msg.get("USERNAME"));
			}

		} catch (Exception e) {
			logger.throwing(this.getClass().getName(), "registerHandler", e);
		}
	}


	/**
	 * Method to get the username of the user logging in.
	 * @return the username of the user.
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * Method to set the variable isLoggedIn to true with the username of the user.
	 * @param username containing the username of the user logged in.
	 */
	public void setLoggedIn(String username, String token, int id) {
		this.username = username;
		this.User.setLoggedIn(true);
		this.User.setToken(token);
		this.User.setUserName(username);
		this.User.setID(id);

		/**
		 * Let the user join the main channel - It will always have ID 1, as its created when the server startsup.
		 */
		server.addUserToGroup(id,1);




	}
	/**
	 * Method that returns the value of isLoggedIn(true or false) due to what the users state is.
	 * @return true if user is logged in, false if not.
	 */
	public boolean isLoggedIn() {
		return this.User.getLoggedIn();
	}

	/**
	 * Gets the user token.
	 * @return
	 */
	public String getToken() {
		return User.getToken();
	}

	/**
	 * Sends a msg to the user, given:
	 * @param groupID
	 * @param msg
	 * @param userName
	 */
	public void sendMsg(int groupID, String msg, String userName) {
		HashMap<String, Object> message = new HashMap<String, Object>();

		message.put("MESSAGE",msg);
		message.put("TYPE", "CHATMESSAGEREPLY");
		message.put("TOKEN", this.getToken());
		message.put("USERNAMEOFSENDER", userName);
		message.put("GROUPID", groupID);
		message.put("ID", groupID);

		try {

			OS.writeObject(message);
			OS.flush();
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendMsg", e);
		}
	}
	
	/**
	 * Sending the message to client
	 * @param msg to send
	 */
	public void sendMessage(HashMap<String,Object> msg) {
		System.out.print("Sending from server " + msg.get("TYPE") + "\n");
		try {
			OS.writeObject(msg);
		} catch (IOException e) {
			logger.throwing(this.getClass().getName(), "sendMessage", e);
		}
	}
	/**
	 * Method to generate a random 64bit String to use as identification between client and server. This is a safer way
	 * then using password and username each time a call is made.
	 * @return the random 64bit string
	 */
	public String generateToken() {
		Random ran = new Random();
		byte[] r = new byte[256];
		ran.nextBytes(r);
		String s = new String(Base64.getEncoder().encode(r));
		
		return s;
	}
	/**
	 * Method to log a user in automatically if he has chosen so during register.
	 * @param msg token sent from client at startup. The token is stored in a file.
	 */
	private void autoLoginHandler(HashMap<String, Object> msg) {
		String userToken = (String)msg.get("TOKEN");
		try {
			database = new DatabaseTableModel();
			String[] str = database.autoLogin((String)msg.get("TOKEN"));
			if (str != null) {
				String user = str[0].toString();
				String intStr = str[2].toString();
				int id = Integer.parseInt(intStr);
				
				setLoggedIn(user, userToken, id);
				send("AUTOLOGIN", userToken, user);
			} else {
				logger.info("autoLogin returned null");
			}
		} catch (SQLException e) {
			logger.throwing(this.getClass().getName(), "autoLoginHandler", e);
		}
		
	}

	/**
	 * gets the userID of the user.
	 * @return UserID.
	 */
	public int getUserID() {
		return this.User.getID();
	}
}
