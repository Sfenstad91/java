package no.ntnu.imt3281.ludo.server;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import no.ntnu.imt3281.ludo.client.Globals;
import no.ntnu.imt3281.ludo.logic.HashPasswordException;

/**
 * Class to create hashed password and generate a random salt value which is included in the Hash.
 * Code gotten from https://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
 * @author
 *
 */
public class HashAndSalt {
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);
	
	/**
	 * Method to create a random salt value witch in turn will be used in the md5 method to create a hashed password
	 * @return the random salt value
	 * @throws NoSuchAlgorithmException
	 */
	public static byte[] getSalt() throws NoSuchAlgorithmException {
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		//Array for salt
		byte[] salt = new byte[16];
		//Generating a random salt
		sr.nextBytes(salt);
		//Return salt
		return salt;
	}
	
	/**
	 * Method to hash the password with PBKDF2(Password Based Key Derivation Function 2). 
	 * @param password the password to be hashed
	 * @param salt the salt to be added to the password
	 * @param iterations Specifies how many times the PBKDF2 executes its underlying algorithm. A higher value is safer.
	 * @param keyLength of 256 is safe
	 * @return the hashed password as a byte array.
	 */
	public static String hashPassword(String password) throws HashPasswordException {
		try {
			int iterations = 1000;
			char[] chars = password.toCharArray();
			byte[] salt = getSalt();
			
			
			PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			//SecretKey key = skf.generateSecret(spec);
			byte[] res = skf.generateSecret(spec).getEncoded();
			return iterations + ":" + toHex(salt) + ":" + toHex(res);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException | HashPasswordException e){
			logger.log(Level.INFO, "hashPassword", e);
		}
		return null;
	}
	/**
	 * Method to write the hashed password as a HEX, including the iterations and salt.
	 * @param array is the salt and hashed password as byte arrays.
	 * @return the hex string (which is the hashed password)
	 * @throws NoSuchAlgorithmException
	 */
	private static String toHex(byte[] array ) throws NoSuchAlgorithmException {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
			if(paddingLength > 0) {
				String fmt = String.format("%%0%dd",paddingLength);
				return String.format(fmt, 0) + hex;
			} else {
			return hex;
			}
	}	
	/**
	 * Method that takes the original password written by the user and the stored password which is in the database.
	 * It checks if two are a match or not, if they are a match they will return a difference of 0 which in turn is a boolean true.
	 * @param originalPassword written by the user
	 * @param storedPassword stored in the database
	 * @return diff == 0(true) if it is a match between the passwords 
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	static boolean validatePassword(String originalPassword, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String [] parts = storedPassword.split(":");
		int iterations = Integer.parseInt(parts[0]);
		byte[] salt = fromHex(parts[1]);
		byte[] hash = fromHex(parts[2]);
		
		PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] testHash = skf.generateSecret(spec).getEncoded();
		
		int diff = hash.length ^ testHash.length;
			for (int i = 0; i < hash.length && i < testHash.length; i++) {
				diff |= hash[i] ^ testHash[i];
			}
		return diff == 0;
	}
	/**
	 * Method to change back the password from hex, so that it may be checked and validated for the user.
	 * @param hex the hex string made by the hashPassword method.
	 * @return the hex value as a byte array.
	 * @throws NoSuchAlgorithmException
	 */
	private static byte[] fromHex(String hex) throws NoSuchAlgorithmException {
		byte[] bytes = new byte[hex.length() / 2];
			for (int i = 0; i < bytes.length; i++) {
				bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
			}
		return bytes;
	}
}
