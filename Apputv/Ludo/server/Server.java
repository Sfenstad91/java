package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.client.Globals;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * 
 * This is the main class for the server. 
 * **Note, change this to extend other classes if desired.**
 * 
 * @author 
 *
 */

public class Server {
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);

	private static ArrayList<ServerToClient> clients = new ArrayList<>();
	private ArrayList<Game> games = new ArrayList<>();
	private ServerResponseReader reader;
	private boolean stop = false;
	private static Server server;
	private static DatabaseTableModel database;

	/**
	 * Method to send chat messages to the group.
	 * @see no.ntnu.imt3281.ludo.server.ServerToClient#sendMsg(int, String, String)
	 * @param groupID id of group
	 * @param msg message sent from client
	 * @param UserName username connected to message
	 */
	// Refactor into functions...
	public void pushNewMessageToGroup(int groupID, String msg, String UserName) {
		ArrayList<ServerToClient> users =  getUsersInGroup(groupID);
		for(ServerToClient user : users ) {
			user.sendMsg(groupID, msg, UserName);
		}
	}

	/**
	 * Returns all members in a given group, including your self
	 * @param groupID
	 * @param userID
	 * @return ArrayList of usernames
	 */
	public ArrayList<String> getUserNameInGroup(int groupID, int userID) {
		ArrayList<ServerToClient> users = getUsersInGroup(groupID);
		ArrayList<String> userList = new ArrayList<>();
		/**
		 * If this member is not part of the group, return null
		 */
		if(!users.contains(getClientFromID(userID))) return null;
		/**
		 * Add all the groupmembers names, to out output array
		 */
		for(ServerToClient user:users)
			userList.add(user.getUsername());

		return userList;
	}


	/**
	 *  Channel that must consist of two or more tokens.
	 * @author
	 *
	 */
	static class Channel{
		Channel(int newgroupID){
			tokens = new ArrayList<>();
			groupID = newgroupID;

		}
		Channel(int member1, int member2, int newgroupID){
			tokens = new ArrayList<>();
			tokens.add(member1);
			tokens.add(member2);
			groupID = newgroupID;
		}
		ArrayList<Integer> tokens;
		int groupID;
	}
	// TODO - add lock
	private ArrayList<Channel> channels = new ArrayList<>();

	/**
	 * Gets a list of available channels for user
	 * @param userid identificator of user
	 * @param isMember of channel or not
	 * @return list of channels
	 */
	public ArrayList<Integer> getChannelsWhere(int userid, boolean isMember){
		ArrayList<Integer> ChannelList= new ArrayList<>();
		for(Channel chan: channels){
			if(isMember && chan.tokens.contains(userid))
				ChannelList.add(chan.groupID);
			else if(!isMember && !chan.tokens.contains(userid))
				ChannelList.add(chan.groupID);
		}

		return ChannelList;

	}
	/**
	 * Checks if a user is one of the group members
	 * @param token
	 * @param groupID
	 * @return true if is a member
	 */
	public boolean isUserInChannel(int token, int groupID){
		for(Channel channel: channels){
			if(channel.groupID == groupID && channel.tokens.contains(token)) return true;
		}
		return false;
	}
	/**
	 * Gets all active clients on server
	 * If the user give us a wrong username, it will still return the list. By design
	 * @param conMsg message recieved from client
	 * @return list of all client who are online
	 */
	public ArrayList<String> getActiveClients(HashMap<String, Object> conMsg){
		ArrayList<String> ClientList=new ArrayList<String>();
		ServerToClient self = getClientFromPlayer((String)conMsg.get("USERNAME"));

		for(ServerToClient client: clients){
			if(self == null || !self.equals(client)) {
				ClientList.add(client.User.getUserName());
			}
		}
		return ClientList;
	}
	/**
	 * Adds a user to the group, if he is not a member
	 * @param token
	 * @param groupID
	 * @return true if added
	 */
	public boolean addUserToGroup(int token, int groupID){
		for(Channel channel: channels){
			if(channel.groupID == groupID &&  !channel.tokens.contains(token) ) {
				channel.tokens.add(token);
				return true;
			}
		}
		return false;
	}

	/**
	 * Removes a user from a group.
	 * @param token
	 * @param groupID
	 * @return true if removed
	 */
	public boolean removeUserFromGroup(int token, int groupID){
		for(Channel channel: channels){
			if(channel.groupID == groupID &&  channel.tokens.contains(token) ) {
				channel.tokens.remove(token);
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates a new chatGroup. Takes the two users as as arguments.
	 * @param token
	 * @param tokenSecondUser
	 * @return the channelID or -1
	 */
	public int makeGroupWith(int token,int tokenSecondUser){
		// DO LOCK here
		int newChannelID = channels.size()+1;
		// TODO We need to look before we do this.
		if(channels.add(new Channel(token, tokenSecondUser, newChannelID))){
			// REMOVE lock here
			return newChannelID;
		}

		// REMOVE lock here.
		return -1;
	}
	/**
	 * Constructor for the server, Creating a socket and accepting new clients
	 * @throws IOException
	 */
	public Server() throws IOException {
		try {
			database = new DatabaseTableModel();
			// We need to add a new channel, for the user to join
			//makeGroupWith("TOKENSUCCESS", "TOKENFAILURE");
			createEmptyChatChannel();

			this.reader = new ServerResponseReader(this);
			new Thread(reader).start();
		
			ServerSocket listener = new ServerSocket(ClientConnection.SERVER_PORT);
			while(!stop) {
				Socket socket = listener.accept(); // this waits for a new connection
				// Each new connection - need its own thread
				ServerToClient newClient = new ServerToClient(socket, this);
				newClient.start();
				clients.add(newClient);
			} 
			listener.close();
		} catch (SQLException e) {
			logger.throwing(this.getClass().getName(), "Server", e);
		}
	}
	/**
	 * @return a vector with all connected clients
	 */
	public ArrayList<ServerToClient> getClients() {
		return clients;
	}
	/**
	 * Method to stop the connection with the reader.
	 */
	protected void stop() {
		if(reader != null)
			reader.stop();
		this.stop = true;
	}
	/**
     * Main function to start the program
     * @param args
     */
	public static void main(String[] args) {
		try {
			server = new Server();
		} catch (IOException e) {
			logger.throwing(Server.class.getClass().getName(), "main", e);
		} 
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				server.stop();
			}
		});

	}
	/**
	 * Creating a new private game, and inviting users
	 * @param conMsg the message recieved from client
	 * @param the client of user
	 */
	public void createPrivateGame(HashMap<String, Object> conMsg, ServerToClient client) {
		//TODO fix this ugly shiet
		int chatID = createEmptyChatChannel();
		Game game = new Game("CLOSED", chatID);
		game.addPlayer(client);
		addUserToGroup(client.getUserID(),game.getChatID());
		String str = (String)conMsg.get("USERNAME");
		str = str.replace("[", "");
		str = str.replace("]", "");
		str = str.replace(",", "");
		String[] players = str.split(" ");
		for(String player : players) {
			if(getClientFromPlayer(player) != null)
				game.invitePlayer(getClientFromPlayer(player), client);
		}
		games.add(game);
	}

	private int createEmptyChatChannel() {
		int newChannelID = channels.size()+1;
		// TODO We need to look before we do this.
		if(channels.add(new Channel(newChannelID))){
			// REMOVE lock here
			return newChannelID;
		}

		// REMOVE lock here.
		return -1;
	}

	/**
	 * Returns a client based on the clients username
	 * @param player we are looking for
	 * @return the client that matches the player
	 */
	private ServerToClient getClientFromPlayer(String player) {
		for(ServerToClient client : clients) {
			if(client.getUsername().equals(player))
				return client;
		}
		return null;
	}
	/**
	 * Method to return a client id from a players name
	 * @param player name
	 * @return client identificator or -1 if failed
	 */
	public int getClientIDFromPlayer(String player) {
		ServerToClient client = getClientFromPlayer(player);
		if (client!=null)
			return client.getUserID();
		return -1;
	}


	/**
	 * Adding player that accepted the game invite to game
	 * @param msg the message recieved
	 * @param the client of current user 
	 */
	public void acceptInvite(HashMap<String, Object> msg, ServerToClient client) {
		for (Game game : games) {
			if(game.getId().equals(msg.get("ID")) && game.playersCanJoin(client)) {
				game.addPlayer(client);
				addUserToGroup(client.getUserID(),game.getChatID());
				if(game.isFull())
					game.startGame();
			}
				
					
		}
	}
	/**
	 * Adding player to a new game, creating a new one if there is none to join
	 * @param msg the message recieved from client
	 * @param client of sender
	 */
	public void joinRandomGame(HashMap<String, Object> msg, ServerToClient client) {
		boolean gameFound = false;
		for (Game game : games) {
			if (game.playersCanJoin(client)) {
				game.addPlayer(client);
				addUserToGroup(client.getUserID(),game.getChatID());
				gameFound = true;
				if (game.isFull()) {
					game.startGame();
				}
			}
		}
		if (!gameFound){
			int chatID = createEmptyChatChannel();
			Game game = new Game(chatID);
			game.addPlayer(client);
			addUserToGroup(client.getUserID(),chatID);
			games.add(game);
		}
	}

	/**
	 * Sends a message to all the members of a channel group
	 * Currently we use this for telling people that a group was created
	 * @param channelID
	 * @param msg
	 */
	public void sendMsgToChannel(int channelID,HashMap<String,Object> msg){
		ArrayList<ServerToClient> clients = getUsersInGroup(channelID);
		for(ServerToClient client:clients)
			client.sendMessage(msg);
	}

	/**
	 * Returns a list of members in a given group
	 * @param groupID
	 * @return ArrayList of Clients
	 */
	public ArrayList<ServerToClient> getUsersInGroup(int groupID) {
		ArrayList<ServerToClient> members = new ArrayList<>();
		Channel channel = getchannel(groupID);
		for(int id:channel.tokens) // Tokens is userID
		{
			ServerToClient client = getClientFromID(id);
			if(client!=null)
				members.add(client);
		}
		return members;
	}

	/**
	 * returns the username of a online player, given the playerID
	 * @param playerID
	 * @return Username or null if players Client object is not found
	 */
	private ServerToClient getClientFromID(int playerID) {
		for(ServerToClient client : clients) {
			if(client.User.getID() == playerID)
				return client;
		}
		return null;
	}

	/**
	 * Returns the correct channel object, given its ID
	 * @param id
	 * @return Channel Object or NULL
	 */
	private Channel getchannel(int id) {
		for (Channel channel : channels) {
			if (channel.groupID == id)
				return channel;
		}
		return null;
	}
	/**
	 * Removing a client that have disconnected
	 * @param username of client that disconnected
	 */
	public void clientDisconnect(String username) {
		clients.remove(getClientFromPlayer(username));
	}
	
}
