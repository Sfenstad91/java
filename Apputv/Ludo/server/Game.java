package no.ntnu.imt3281.ludo.server;

import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.client.Globals;
import no.ntnu.imt3281.ludo.logic.*;

/**
 * This is the Game class. It handles all the events and requests from the game. Without this class there wouln't be any game to play.
 * It implements PieceListener, PlayerListener and DiceListener to handle the events of players joining, dice being thrown and pieces being moved etc.
 * @see no.ntnu.imt3281.ludo.logic.PieceListener
 * @see no.ntnu.imt3281.ludo.logic.PlayerListener
 * @see no.ntnu.imt3281.ludo.logic.DiceListener
 * @author Group
 *
 */
public class Game implements PieceListener, PlayerListener, DiceListener {
	private static Logger logger = Logger.getLogger(Globals.LOG_NAME);

	private ArrayList<ServerToClient> players = new ArrayList<>(4);
	private ArrayList<ServerToClient> invitedPlayers = new ArrayList<>(3);
	private String status = "WAITING";
	private String type = "OPEN";
	private String id;
	private Ludo ludoGame;
	private int Counter = 0;
	private int eventNr = 0;
	private int timeout = 60; // A player have 1 min to finish his turn.
	private int chatID =-1;
	private boolean firstTime = true;
	DatabaseTableModel database;

	/**
	 * Creating a new game that is open for new players
	 * @param chatID the id of the Chat
	 */
	public Game(int chatID) {
		this.chatID = chatID;
		this.id = String.valueOf(Counter++);
		ludoGame = new Ludo();
		initGameListners();
	}

	/**
	 * Adds dice,piece,and player listener for local events.
	 */
	private void initGameListners(){
		ludoGame.addDiceListener(this);
		ludoGame.addPlayerListener(this);
		ludoGame.addPieceListener(this);
	}

	/**
	 * Adds listeners for events from clients.
	 * @param playerID id of the player
	 * @param client
	 */
	private void initListners(int playerID, ServerToClient client) {
		GameListener diceThrown  = data -> throwDiceHandler(data,playerID);
		GameListener movePiece  = data -> movePieceHandler(data,playerID);

		client.addGameListner("MOVEPIECE", movePiece);
		client.addGameListner("THROWDICE", diceThrown);

	}

	/**
	 * Handles movepiece events from the clients.
	 * First makes sure that the move is allowed, and that this player is the active one
	 * and then do the move.
	 * @param data map of GameID, and piece the user want to move.
	 * @param playerID - if of the player doing the move
	 */
	private void movePieceHandler(Map<String, Object> data, int playerID) {
		if((data.get("GAMEID").equals(this.id)) &&
				(playerID == ludoGame.activePlayer() )){
			int piece = (int) data.get("PIECE");
			System.out.println("Piece - " + piece);
			int pos = ludoGame.getPosition(playerID,piece );

			if(!ludoGame.canMove()) return;
			// Use the banlist canMove creates to check if that piece can move.
			// E.g its not blocked
			if(!ludoGame.canMove(piece)) return;
			int newpos = pos + ludoGame.getlastDiceValue();
			// if we are moving out from start, we move to position 1
			if(pos == 0) newpos =1;
			// In movePiece we should also clear lastDiceValue;
			ludoGame.movePiece(playerID,pos,newpos);
		}

	}
	/**
	 * This method handles the throw dice event.
	 * @param data map of GameID
	 * @param playerID id of the player
	 */
	private void throwDiceHandler(Map<String, Object> data, int playerID) {
		if(data.get("GAMEID").equals(this.id)){
			if(playerID == ludoGame.activePlayer() ){
				// tell the server that we did a throwEvent.
				ludoGame.throwDice();
			}
		}
	}

	/**
	 * Creating a new game where the type is sent with as a parameter
	 * @param type of the game. Either Random game request or private game
	 * @param chatID id of the chat
	 */
	public Game(String type, int chatID) {
		this.chatID = chatID;
		this.type = type;
		this.id = String.valueOf(Counter++);
		ludoGame = new Ludo();
		initGameListners();

	}

	/** checks how many players is in the game.
	 * @return number of players in game
	 */
	public int getPlayers() {
		return players.size();
	}
	/**
	 * Test to see if it is a open game.
	 * @return open game
	 */
	public boolean isOpen() {
		return type.equals("OPEN");
	}
	/**
	 * Checks to see if the client can join a game
	 * @param client
	 * @return if the client can join
	 */
	public boolean playersCanJoin(ServerToClient client) {
		if(this.status.equals("STARTED"))
			return false;
		else if(players.indexOf(client) != -1)
			return false;
		else if(isOpen() && players.size() <= 4)
			return true;
		else if(!isOpen() && playerIsInvited(client))
			return true;
		else
			return false;
	}
	/**
	 * Checks if a client is invited to a game
	 * @param client
	 * @return if clients were invited
	 */
	private boolean playerIsInvited(ServerToClient client) {
		if (invitedPlayers.indexOf(client) > -1)
			return true;
		else
			return false;
	}
	/**
	 * @return if the game is full.
	 */
	public boolean isFull() {
		return players.size() == 4;
	}
	/**
	 * Adding a player to the game
	 * @param client
	 */
	public void addPlayer(ServerToClient client) {
		sendMessageToClient("NEWJOINEDGAME", "", client);
		// Update client with all people already in game
		for (ServerToClient player : players) {
			sendMessageToClient("PLAYERJOINED", player.getUsername(), client);
		}

		// Add this player to the game
		players.add(client);
		ludoGame.addPlayer(client.getUsername());

		// Tell everyone in game that this player joined
		for (ServerToClient player : players) {
			sendMessageToClient("PLAYERJOINED", client.getUsername(), player);
		}
		// Add listners for that client.
		initListners(getPlayerIDByName(client.getUsername()), client);
	}
	/**
	 * Method to get the playerID by the name sent in parameter.
	 * @param name name of player used to collect the ID of the player
	 * @return playerID
	 */
	private int getPlayerIDByName(String name){
		for(int playerID = 0; playerID<4; playerID++)
			if(ludoGame.getPlayerName(playerID).equals(name))
				return playerID;
		return -1;
	}



	/**
	 * Invite a player to the game
	 * @param client to invite
	 * @param inviter who invites the client/player
	 */
	public void invitePlayer(ServerToClient client, ServerToClient inviter) {
		sendMessageToClient("GAMEINVITE", inviter.getUsername() , client);
		invitedPlayers.add(client);
	}

	/**
	 * Sending a game message to the client specified as a parameter
	 * @param msg type of message
	 * @param client which we are sending the message to
	 */
	private void sendMessageToClient(String msg, String username, ServerToClient client) {
		HashMap<String,Object> message = new HashMap<String,Object>();
		message.put("TYPE", msg);
		message.put("USERNAME", username);
		message.put("ID", this.id);
		message.put("PLAYERSIZE", players.size());
		message.put("CHATID", this.chatID);
		client.sendMessage(message);
	}


	/**
	 * Method to send message to all active clients.
	 * @param msg message to send
	 */
	private void sendToAllClients(HashMap<String, Object> msg){
		for (ServerToClient player : players) {
			sendMessageToClient(msg, player);
		}
	}
	/**
	 * Send a message to a client
	 * @param msg
	 * @param client
	 */
	private void sendMessageToClient(HashMap< String, Object> msg, ServerToClient client) {
		msg.put("ID", this.id);
		client.sendMessage(msg);
	}


	/**
	 * Called when a dice is thrown,
	 * sending message to the clients with the information from the event.
	 * @param
	 */
	@Override
	public void diceThrown(DiceEvent event) {
		HashMap<String, Object> msg = new HashMap<>();
		msg.put("TYPE", "DICETHROWN");
		msg.put("COLOR", event.getPlayer());
		msg.put("DICEVALUE", event.getDice());
		sendToAllClients(msg);
		System.out.println("Sent DiceThrown to server");

	}

	/**
	 * Called when the state of a player is changed,
	 * sending message to the clients with the new state
	 */
	@Override
	public void playerStateChanged(PlayerEvent event) {
		this.eventNr++;

		/**
		 * If a user won, we handle it on the server to log/save the user who won.
		 */
		if(event.getState() == PlayerEvent.WON){
			status = "FINISHED";

			addScore(event);
		}
		else if(event.getState() == PlayerEvent.LEFTGAME){


		}
		else if (event.getState() == PlayerEvent.PLAYING){
			/**
			 * The first time someone play a move, set up a  Timeout listner
			 * Currently it will timeout in 60seconds.
			 */
			if(this.firstTime){
				new TreadTimeout(this,60).start();
				firstTime = false;
			}
			/**
			 * Check if the new player have sent us any ping request lately
			 * if not, he is inactive.
			 */
			if(players.get(event.getActivePlayer()).getlastPingTime().isBefore(Instant.now().minusSeconds(30)))
				playerIsInactive(event.getActivePlayer());

		}

	}
	/**
	 * Adds score to players who finished and won a game
	 * @param event when a game finished, someone won.
	 */
	private void addScore(PlayerEvent event) {
		try {
			database = new DatabaseTableModel();
			database.incrementGamesWon(ludoGame.getPlayerName(ludoGame.getWinner()));
			for(int player = 0; player < ludoGame.activePlayers(); player++)
				database.incrementGamesPlayed(ludoGame.getPlayerName(player));
		} catch (SQLException e) {
			logger.throwing(this.getClass().getName(), "addScore", e);
		}

	}

	/**
	 * Triggered when a piece is moved
	 * sending messages to the players about the move
	 */
	@Override
	public void pieceMoved(PieceEvent event) {
		HashMap<String, Object> msg = new HashMap<>();

		msg.put("TYPE", "PIECEMOVED");
		msg.put("COLOR",event.getPlayer());
		msg.put("PIECE",event.getPiece());
		msg.put("FROM",event.getFrom());
		msg.put("TO",event.getTo());

		sendToAllClients(msg);
		System.out.println("Sent pieceMoved to server");

	}


	/**
	 * @return current game id
	 */
	public String getId() {
		return id;
	}
	/**
	 * Starting the game
	 */
	public void startGame() {
		HashMap<String, Object> msg = new HashMap<>();

		this.status = "STARTED";
		msg.put("TYPE", "STARTGAME");
		sendToAllClients(msg);
	}


	/**
	 * Tell you if any more events have happened since last time you got an eventNr.
	 * @param eventNr
	 * @return
	 */
	public boolean hasNotChanged(int eventNr) {
		return this.eventNr == eventNr;
	}


	/**
	 * If a player is not doing anything for 60 seconds. Remove him.
	 * @param activePlayer
	 */
	public void playerIsInactive(int activePlayer) {

		// We also need to remove his pieces on the server:
		ludoGame.removePlayer(ludoGame.getPlayerName(activePlayer));
		ludoGame.removePlayerPieces(activePlayer);

		/**
		 * tell all the players about the fact that one player is no longer playing
		 * Either he just didnt do any move for X seconds or his connection went away.
		 */
		HashMap<String, Object> msg = new HashMap<>();
		msg.put("TYPE", "STATECHANGE");
		msg.put("PLAYER", activePlayer);
		msg.put("STATE", PlayerEvent.LEFTGAME);
		sendToAllClients(msg);

		ludoGame.nextPlayer();
		ludoGame.weAlone();

	}
	/**
	 * method that returns id of chat
	 * @return
	 */
	public int getChatID() {
		return chatID;
	}
	/**
	 * Checks if a player in game is inactive or havent done anything for 60 seconds.
	 */
	public void playerIsInactive() {
		playerIsInactive(ludoGame.activePlayer());
	}
	/**
	 * get the event number.
	 * @return event number
	 */
	public int geteventNr() {
		return eventNr;
	}
	/**
	 * Gets status of game, sees if it is finsihed or in progress
	 * @return status of the game
	 */
	public String getStatus() {
		return status;
	}
}