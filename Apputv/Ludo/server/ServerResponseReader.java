package no.ntnu.imt3281.ludo.server;

/**
 * This Class will be the link between the messages from server and clients
 * @author 
 *
 */
public class ServerResponseReader implements Runnable{
	private Server server;
	private boolean stop = false;
	/**
	 * Constructor of the class ServerResonseReader. It sets the server instance to the parameter.
	 * @param server sent from Server
	 */
	public ServerResponseReader(Server server) {
		this.server = server;
	}
	/**
	 * Run method of the class ServerResponseReader.
	 */
	@Override
	public void run() {
		
	}

	/**
	 * Method to set the variable stop to true.
	 */
	public void stop() {
		this.stop = true;
	}
}
